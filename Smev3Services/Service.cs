﻿using CryptoPro.Sharpei;
using SMEV3Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Xml;
using System.Xml.Serialization;


namespace Smev3Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service : IService
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        //Ftp-хранилище
        #region RSID30042

        public CommonResponse rsid30042req(ref CommonRequest req)
        {
            if (String.IsNullOrEmpty(req.StreetCode) || String.IsNullOrEmpty(req.StreetName) ||
                String.IsNullOrEmpty(req.HouseCode) || String.IsNullOrEmpty(req.HouseName))
                throw new Exception("Не все обязательные поля заполнены!");

            CommonRequest sreq = new CommonRequest(req);
            sreq.Region = req.Region;
            sreq.Locality = req.Locality;
            sreq.Town = req.Town;
            sreq.StreetCode = req.StreetCode;
            sreq.StreetName = req.StreetName.Trim();
            sreq.HouseCode = req.HouseCode;
            sreq.HouseName = req.HouseName.Trim();
            sreq.BuildingCode = req.BuildingCode;
            sreq.Building = req.Building;
            sreq.StructureCode = req.StructureCode;
            sreq.Structure = req.Structure;
            if (!String.IsNullOrEmpty(req.Flat))
            {
                sreq.FlatCode = req.FlatCode;
                sreq.Flat = req.Flat;
            }
            // по заявке SUB-607
            sreq.GUID = req.GUID;

            req = sreq;

            if ((req.IsDouble == "0") || (req.IsDouble == "2"))
            {
                List<int> doubles = Connections.DoubleSearch(req);
                if ((doubles.Count > 0) || (req.IsDouble == "2")) return new CommonResponse() { DoubleRequests = doubles };
            }

            Connections.SetTabNumber(WithIP(req));

            #region Client Settings

            SMEV3Services.SMEVMessageExchangePortTypeClient client = new SMEV3Services.SMEVMessageExchangePortTypeClient();

            CustomBinding binding = new CustomBinding(client.Endpoint.Binding);
            binding.SendTimeout = new TimeSpan(0, 2, 0);

            SMEVRequest sr = new SMEVRequest();
            SMEVMessageEncodingBindingElement ebe = new SMEVMessageEncodingBindingElement("30042", req, sr, 13);
            binding.Elements.Remove<MtomMessageEncodingBindingElement>();
            binding.Elements.Insert(0, ebe);

            client.Endpoint.Binding = binding;

            #endregion

            #region Data Settings

            SMEV3Services.SenderProvidedRequestData rr = new SMEV3Services.SenderProvidedRequestData();
            rr.Id = "SIGNED_BY_CALLER";
            rr.MessageID = GuidGenerator.GenerateTimeBasedGuid().ToString();

            string idPassport = Guid.NewGuid().ToString();

            EGRN.TEGRNRequest er = new EGRN.TEGRNRequest()
            {
                header = new EGRN.TEGRNRequestHeader()
                {
                    actionCode = EGRN.DActionCode.Item659511111112,
                    actionCodeSpecified = true,
                    statementType = "558630200000",
                    creationDate = DateTime.Now,
                    creationDateSpecified = true,
                    appliedDocument = new List<EGRN.TBasicSomeDocument>()
                    {
                        new EGRN.TBasicSomeDocument()
                        {
                            Item = new EGRN.TOtherDocumentRestr()
                            {
                                _id = Guid.NewGuid().ToString(),
                                documentTypes = new List<EGRN.DocumentTypes>()
                                {
                                    new EGRN.DocumentTypes()
                                    {
                                        ItemElementName = EGRN.ItemChoiceType.documentTypeCode,
                                        Item = EGRN.DDocument.Item558630200000//Item558102100000
                                    }
                                },
                                number = req.CallID
                            }
                        },
                        new EGRN.TBasicSomeDocument()
                        {
                            Item = new EGRN.TIdDocumentRestr()
                            {
                                _id = idPassport,
                                documentTypes = new List<EGRN.DocumentTypes>()
                                {
                                    new EGRN.DocumentTypes()
                                    {
                                        ItemElementName = EGRN.ItemChoiceType.documentTypeCode/* EGRN.ItemChoiceType.PassportRF*/,
                                        Item = EGRN.DDocument.Item008001001000
                                    }
                                },
                                series = "4011",
                                number = "273039",
                                issueDateSpecified = true,
                                issueDate = DateTime.Parse("2001-06-17")
                            }
                        }
                    }
                },
                declarant = new EGRN.TEGRNRequestDeclarant()
                {
                    ItemElementName = EGRN.ItemChoiceType2.previligedPerson,
                    Item = new EGRN.TEGRNRequestPreviligedPerson()
                    {
                        surname = "Школяр",
                        firstname = "Ирина",
                        patronymic = "Владимировна",
                        idDocumentRef = new EGRN.TDocumentRefer() { documentID = idPassport },
                        snils = "011-610-715 84"
                    },
                    declarantKind = "357013000000"
                },
                requestDetails = new EGRN.TEGRNRequestDetails()
                {
                    Item = new EGRN.TRequestDataAction()
                    {
                        Item = new EGRN.TExtractDataAction()
                        {
                            @object = new List<EGRN.TEGRNRequestObject>()
                            {
                                new EGRN.TEGRNRequestObject()
                                {
                                    objectTypeCode = "002001003000",
                                    address = new List<EGRN.TAddress>
                                    {
                                        new EGRN.TAddress()
                                        {
                                            kladr = "78000000000154800",
                                            region = new EGRN.TAddressElement() { code = "78" },
                                            street = new EGRN.TAddressElement4Street() { name = req.StreetName, type = req.StreetCode },
                                            house = new EGRN.THouse() { type = EGRN.DHouse.д, value = req.HouseName },
                                            apartment = new EGRN.TApartment() { type = EGRN.DApartment.кв, typeSpecified = true, name = req.Flat }
                                        }
                                    }
                                }
                            },
                            requestType = EGRN.DRequestDataType.extractRealty
                        }
                    }
                },
                deliveryDetails = new EGRN.TEGRNRequestDeliveryDetails()
                {
                    resultDeliveryMethod = new EGRN.TBasicResultDeliveryMethod()
                    {
                        recieveResultTypeCode = "webService"
                    }
                },
                statementAgreements = new EGRN.TEGRNRequestAgreements()
                {
                    actualDataAgreement = EGRN.DActualDataAgreements.Item04,
                    persDataProcessingAgreement = EGRN.DPersDataAgreements.Item01,
                    qualityOfServiceAgreement = EGRN.DQualityOfServiceAgreements.Item01,
                    qualityOfServiceTelephoneNumber = "2461855"
                }
            };


            Directory.CreateDirectory(@"D:\IISFilesCrypt\" + rr.MessageID);

            string fileName = Guid.NewGuid().ToString() + ".xml";
            XmlDocument reqData = Serialize(typeof(EGRN.TEGRNRequest), er, null);
            reqData.Save(@"D:\IISFilesCrypt\" + rr.MessageID + @"\" + fileName);

            EGRN.TRequest tr = new EGRN.TRequest()
            {
                statementFile = new List<EGRN.TStatementFile>()
                {
                    new EGRN.TStatementFile() { fileName = fileName }
                },
                file = new List<EGRN.TFile>()
                {
                    new EGRN.TFile() { fileName = fileName + ".sig" }
                },
                requestType = EGRN.DRequestType.Item111300003000
            };

            reqData = Serialize(typeof(EGRN.TRequest), tr, null);
            reqData.Save(@"D:\IISFilesCrypt\" + rr.MessageID + @"\Request.xml");

            CRYPTO.Service cs = new CRYPTO.Service();
            string signData = cs.GetDetachedStreamSign(rr.MessageID + @"\" + fileName, clientCertificate28().SerialNumber, false);
            string signReq = cs.GetDetachedStreamSign(rr.MessageID + @"\Request.xml", clientCertificate28().SerialNumber, false);
            string zipFile = cs.GetArchiveStr(
                new string[] { rr.MessageID + @"\" + fileName, rr.MessageID + @"\Request.xml", rr.MessageID + @"\" + signData, rr.MessageID + @"\" + signReq },
                CRYPTO.ArchiveType.ZIP);

            ROSREE.requestType rq = new ROSREE.requestType()
            {
                region = ROSREE.dRegionsRF.Item78,
                senderType = ROSREE.senderTypes.Vedomstvo,
                actionCode = ROSREE.DActionCode.Item659511111112,
                actionCodeSpecified = true,
                Attachment = new ROSREE.AttachmentRequestType()
                {
                    ItemElementName = ROSREE.ItemChoiceType.AttachmentFSLink,
                    Item = true,
                    RequestDescription = new ROSREE.TValidatedStructuredAttachmentFormat()
                    {
                        IsUnstructuredFormat = false,
                        IsZippedPacket = true,
                        fileName = "request.xml"
                    },
                    Statement = new List<ROSREE.TValidatedStructuredAttachmentFormat>()
                    {
                        new ROSREE.TValidatedStructuredAttachmentFormat()
                        {
                            IsUnstructuredFormat = false,
                            IsZippedPacket = true,
                            fileName = fileName
                        }
                    },
                    File = new List<ROSREE.TStructuredAttachmentFormat>()
                    {
                        new ROSREE.TStructuredAttachmentFormat()
                        {
                            IsUnstructuredFormat = true,
                            IsZippedPacket = true,
                            fileName = fileName + ".sig"
                        },
                        new ROSREE.TStructuredAttachmentFormat()
                        {
                            IsUnstructuredFormat = true,
                            IsZippedPacket = true,
                            fileName = "request.xml.sig"
                        }
                    }
                }
            };

            XmlDocument xd = Serialize(typeof(ROSREE.requestType), rq, null);
            rr.MessagePrimaryContent = xd.DocumentElement;

            byte[] fileContent = File.ReadAllBytes(@"D:\IISFilesCrypt\" + zipFile);
            byte[] hash = Gost3411_2012_256.Create().ComputeHash(fileContent);
            System.Security.Cryptography.Pkcs.ContentInfo cInfo = new System.Security.Cryptography.Pkcs.ContentInfo(hash);
            System.Security.Cryptography.Pkcs.SignedCms sCms = new System.Security.Cryptography.Pkcs.SignedCms(cInfo, true);
            sCms.ComputeSignature(new System.Security.Cryptography.Pkcs.CmsSigner(clientCertificate28()), false);

            rr.RefAttachmentHeaderList = new SMEV3Services.RefAttachmentHeaderType[]
            {
                new SMEV3Services.RefAttachmentHeaderType()
                {
                    uuid = rr.MessageID,
                    Hash = Convert.ToBase64String(hash),
                    MimeType = "application/zip",
                    SignaturePKCS7 = sCms.Encode()
                }
            };

            xd = Serialize(typeof(SMEV3Services.SenderProvidedRequestData), rr, null);
            XmlElement callerSignature = SignedData(xd, rr.Id);
            XmlElement smevSignature;

            #endregion

            Response result = new Response();

            try
            {
                FtpWebRequest ftpreq = (FtpWebRequest)WebRequest.Create("ftp://ftp.smev.vpn/" + rr.MessageID);
                ftpreq.Credentials = new NetworkCredential("anonymous", "c2h5oh@smev.vpn");
                ftpreq.Method = WebRequestMethods.Ftp.MakeDirectory;
                ftpreq.GetResponse();

                ftpreq = (FtpWebRequest)WebRequest.Create("ftp://ftp.smev.vpn/" + rr.MessageID + "/" + zipFile);
                ftpreq.Credentials = new NetworkCredential("anonymous", "c2h5oh@smev.vpn");
                ftpreq.Method = WebRequestMethods.Ftp.UploadFile;

                ftpreq.ContentLength = fileContent.LongLength;

                Stream requestStream = ftpreq.GetRequestStream();
                requestStream.Write(fileContent, 0, fileContent.Length);
                requestStream.Close();

                ftpreq.GetResponse();

                SMEV3Services.MessageMetadata mm = client.SendRequest(rr, null, callerSignature, out smevSignature);

                result.MethodResponse = mm;

                if (mm.Status == SMEV3Services.InteractionStatusType.requestIsQueued)
                {
                    req.RequestID = rr.MessageID;
                    req.StateID = 2;
                    Connections.LogAsync(req);
                }
            }
            //catch (WebException ex)
            //{
            //    string Error = ((FtpWebResponse)ex.Response).StatusDescription;

            //    File.WriteAllText(@"D:\temp\err_" + rr.MessageID + "_" + DateTime.Now.ToString("HHmmssfff") + ".log", Error);

            //    result.Error = Error;
            //}
            catch (Exception ex)
            {
                string Error = ex.Message;
                if (ex.InnerException != null) Error += " " + ex.InnerException.Message;

                File.WriteAllText(@"D:\temp\err_" + rr.MessageID + "_" + DateTime.Now.ToString("HHmmssfff") + ".log", Error);

                if (req.StateID != 1)
                {
                    Connections.CheckAndLog(req, sr.Request, Error, true);
                    req.StateID = 1;
                }

                result.Error = Error;
            }

            return new CommonResponse(result, DateTime.Now, DateTime.Now);
        }


        private CommonRequest WithIP(CommonRequest cr)
        {
            cr.IP = ClientIP();
            return cr;
        }

        private string ClientIP()
        {
            OperationContext context = OperationContext.Current;
            MessageProperties prop = context.IncomingMessageProperties;
            RemoteEndpointMessageProperty endpoint = prop[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            return endpoint.Address;
        }

        private XmlDocument Serialize(Type type, object obj, XmlRootAttribute root)
        {
            XmlSerializer xSer = (root == null) ? new XmlSerializer(type) : new XmlSerializer(type, root);

            StringWriter sw = new StringWriter();
            XmlTextWriter xWriter = new XmlTextWriter(sw);

            xSer.Serialize(xWriter, obj);

            xWriter.Flush();
            sw.Flush();

            XmlDocument xDoc = new XmlDocument();
            xDoc.PreserveWhitespace = true;
            xDoc.LoadXml(sw.GetStringBuilder().ToString());

            return xDoc;
        }
        private object Deserialize(XmlDocument xDoc, Type type, XmlRootAttribute root)
        {
            StringReader sr = new StringReader(xDoc.OuterXml);
            XmlSerializer xs = (root == null) ? new XmlSerializer(type) : new XmlSerializer(type, root);
            return xs.Deserialize(sr);
        }

        private XmlElement SignedData(XmlDocument data, string uri)
        {
            SignedXml sXml = new SignedXml(data);

            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);

            Gost3410_2012_256CryptoServiceProvider prov = (Gost3410_2012_256CryptoServiceProvider)clientCertificate28().PrivateKey;

            sXml.SigningKey = prov;

            KeyInfo keyInfo = new KeyInfo();
            keyInfo.AddClause(new KeyInfoX509Data(clientCertificate28()));
            sXml.KeyInfo = keyInfo;

            Reference refer = new Reference();
            refer.Uri = "#" + uri;
            refer.DigestMethod = CPSignedXml.XmlDsigGost3411_2012_256Url;

            //refer.AddTransform(new XmlDsigEnvelopedSignatureTransform(true));
            refer.AddTransform(new XmlDsigExcC14NTransform());
            refer.AddTransform(new XmlDsigSmevTransform());

            sXml.AddReference(refer);

            sXml.SignedInfo.CanonicalizationMethod = new XmlDsigExcC14NTransform().Algorithm;

            sXml.SignedInfo.SignatureMethod = CPSignedXml.XmlDsigGost3410_2012_256Url;

            sXml.ComputeSignature();
            return sXml.Signature.GetXml();
        }

    }
    #endregion
}
