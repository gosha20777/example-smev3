﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMEV3Services.SMEV3
{
    public class MessageMetadata
    {
        public string MessageId;
        public Smev3Services.SMEV3Services.InteractionStatusType Status;
    }
}