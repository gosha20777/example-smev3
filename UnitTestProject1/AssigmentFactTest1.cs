﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using EGISSO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SignatureHelper.ClassesFromXSD.Assigment_Fact;
using SignatureHelper.SaveFilesXML.PathNameFormat;

namespace EGISSOTests
{
    [TestClass]
    public class EGISSOTests
    {
        private static DataTable dt = new DataTable("MyTable");
        public TestContext TestContext { get; set; } 

        [TestMethod]
        public void Check_Elements_XML()
        {

            //Организация создание имитированного хранилища
            var createrAssigmentFact = new CreateAssigmentFact();

            var groupsPackageGuid = dt.Select()
                .OrderBy(r => r["districtID"])
                .ThenBy(t => t["packageID"])
                .ThenBy(r => r["id_familyUD"])
                .ThenBy(r => r["sam"])
                .GroupBy(g => g["packageID"]);



            //Пакет запроса данных Получателя МСЗ

            var i = 0;

            foreach (var packageGuid in groupsPackageGuid)
            {
                i++;
                //Пакет запроса данных Получателя МСЗ
                var packageList = new List<tFactAssignment2>();
                Guid packGuid = Guid.Empty;
                var _fileNameXML = new FileNameXML();

                //Assert.IsNotNull(createrAssigmentFact.GetXMLAssigmFact(packageGuid, packGuid, dt, packageList));
                var test = createrAssigmentFact.GetXMLAssigmFact(packageGuid, packGuid, dt, packageList, _fileNameXML);

                var categoryId = test.package.elements.fact.Select(x => x.categoryId).FirstOrDefault();
                var uuidGuid = test.package.elements.fact.Select(x => x.uuid).FirstOrDefault();

                Assert.IsTrue(categoryId == "9fdbc106-1ab4-4040-9bc1-061ab4c040ed");

                Assert.IsTrue(uuidGuid == "5076f7ca-38cb-49b3-a958-ff688ffe9de3");
            }
        }

        /// <summary>
        /// Инициализация хранилища данных, запускается перед каждым стартом методом. 
        /// </summary>
        /// <param name="dt"></param>
        [ClassInitialize]
        public static void InitializeTest(TestContext context)
        {

            DataColumn districtID = new DataColumn("districtID", typeof(int));
            DataColumn packageID = new DataColumn("packageID", typeof(Guid));
            DataColumn id_familyUD = new DataColumn("id_familyUD", typeof(int));
            DataColumn sam = new DataColumn("sam", typeof(int));
            DataColumn oszCode = new DataColumn("oszCode", typeof(string));
            DataColumn one_fact_family = new DataColumn("one_fact_family", typeof(bool));
            DataColumn short_kod = new DataColumn("short_kod", typeof(string));
            DataColumn name_extension = new DataColumn("name_extension", typeof(string));
            DataColumn usingSign = new DataColumn("usingSign", typeof(bool));
            DataColumn recordID = new DataColumn("recordID", typeof(string));
            DataColumn categoryID = new DataColumn("categoryID", typeof(string));
            DataColumn lmszProviderCode = new DataColumn("lmszProviderCode", typeof(string));
            DataColumn Series = new DataColumn("Series", typeof(string));
            DataColumn Number = new DataColumn("Number", typeof(string));
            DataColumn Issuer = new DataColumn("Issuer", typeof(string));
            DataColumn Region = new DataColumn("Region", typeof(string));
            DataColumn City = new DataColumn("City", typeof(string));
            DataColumn District = new DataColumn("District", typeof(string));
            DataColumn OKSMCode = new DataColumn("OKSMCode", typeof(string));
            DataColumn PostIndex = new DataColumn("PostIndex", typeof(string));
            DataColumn Apartment = new DataColumn("Apartment", typeof(string));
            DataColumn Street = new DataColumn("Street", typeof(string));
            DataColumn House = new DataColumn("House", typeof(string));
            DataColumn Housing = new DataColumn("Housing", typeof(string));
            DataColumn Person_id = new DataColumn("Person_id", typeof(int));
            DataColumn decision_date = new DataColumn("decision_date", typeof(DateTime));
            DataColumn dateStart = new DataColumn("dateStart", typeof(DateTime));
            DataColumn dateFinish = new DataColumn("dateFinish", typeof(DateTime));
            DataColumn IssueDate = new DataColumn("IssueDate", typeof(DateTime));
            DataColumn SNILS = new DataColumn("SNILS", typeof(string));
            DataColumn FamilyName = new DataColumn("FamilyName", typeof(string));
            DataColumn FirstName = new DataColumn("FirstName", typeof(string));
            DataColumn Patronymic = new DataColumn("Patronymic", typeof(string));
            DataColumn Gender = new DataColumn("Gender", typeof(string));
            DataColumn BirthDate = new DataColumn("BirthDate", typeof(DateTime));
            DataColumn Citizenship = new DataColumn("Citizenship", typeof(string));
            DataColumn amount = new DataColumn("amount", typeof(int));
            DataColumn measuryCode = new DataColumn("measuryCode", typeof(string));
            DataColumn content = new DataColumn("content", typeof(string));
            DataColumn comment = new DataColumn("comment", typeof(string));
            DataColumn equivalentAmount = new DataColumn("equivalentAmount", typeof(int));
            DataColumn lmszId = new DataColumn("lmszId", typeof(Guid));
            DataColumn id_assignmentInfo = new DataColumn("id_assignmentInfo", typeof(int));
            DataColumn amount_Monetary = new DataColumn("amount_Monetary", typeof(string));
            DataColumn Viddoc = new DataColumn("Viddoc", typeof(int));


            dt.Columns.Add(districtID);
            dt.Columns.Add(packageID);
            dt.Columns.Add(id_familyUD);
            dt.Columns.Add(oszCode);
            dt.Columns.Add(one_fact_family);
            dt.Columns.Add(sam);
            dt.Columns.Add(short_kod);
            dt.Columns.Add(name_extension);
            dt.Columns.Add(usingSign);
            dt.Columns.Add(recordID);
            dt.Columns.Add(categoryID);
            dt.Columns.Add(lmszProviderCode);
            dt.Columns.Add(Series);
            dt.Columns.Add(Number);
            dt.Columns.Add(Issuer);
            dt.Columns.Add(Region);
            dt.Columns.Add(City);
            dt.Columns.Add(District);
            dt.Columns.Add(OKSMCode);
            dt.Columns.Add(PostIndex);
            dt.Columns.Add(Apartment);
            dt.Columns.Add(Street);
            dt.Columns.Add(House);
            dt.Columns.Add(Housing);
            dt.Columns.Add(Person_id);
            dt.Columns.Add(dateStart);
            dt.Columns.Add(dateFinish);
            dt.Columns.Add(IssueDate);
            dt.Columns.Add(SNILS);
            dt.Columns.Add(FamilyName);
            dt.Columns.Add(FirstName);
            dt.Columns.Add(Patronymic);
            dt.Columns.Add(Gender);
            dt.Columns.Add(BirthDate);
            dt.Columns.Add(Citizenship);
            dt.Columns.Add(amount);
            dt.Columns.Add(measuryCode);
            dt.Columns.Add(content);
            dt.Columns.Add(comment);
            dt.Columns.Add(lmszId);
            dt.Columns.Add(decision_date);
            dt.Columns.Add(id_assignmentInfo);
            dt.Columns.Add(amount_Monetary);
            dt.Columns.Add(equivalentAmount);
            dt.Columns.Add(Viddoc);


            DataRow newRow = dt.NewRow();
            newRow["districtID"] = 1;
            newRow["packageID"] = "3c074c19-8d01-4996-923e-c576d59f3a0b";
            newRow["id_familyUD"] = 1;
            newRow["sam"] = 1;
            newRow["oszCode"] = "1251.000001";
            newRow["one_fact_family"] = false;
            newRow["SNILS"] = "00658240334";
            newRow["FamilyName"] = "Тест";
            newRow["FirstName"] = "Тесторова";
            newRow["Patronymic"] = "Тесторовна";
            newRow["Gender"] = "Female";
            newRow["BirthDate"] = Convert.ToDateTime("1946-01-01");
            newRow["Citizenship"] = 643;
            newRow["lmszId"] = "1a25db8d-a92c-42ae-be2a-c68bb2321dc7";
            newRow["lmszProviderCode"] = 1251.000000;
            newRow["categoryId"] = "9fdbc106-1ab4-4040-9bc1-061ab4c040ed";
            newRow["decision_date"] = DateTime.ParseExact("2019-05-28", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            newRow["dateStart"] = DateTime.ParseExact("2019-05-28", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            newRow["dateFinish"] = DateTime.ParseExact("2019-05-28", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            newRow["usingSign"] = true;
            newRow["id_assignmentInfo"] = 2;
            newRow["amount"] = 1;
            newRow["content"] = "ЖИЛОЕ ПОМЕЩЕНИЕ ПЛОЩАДЬЮ  9999.70 КВ.М ПО АДРЕСУ:";
            newRow["comment"] = "УЧЕТНОЕ ДЕЛО  9999999,  АДМИРАЛТЕЙСКИЙ РАЙОН";
            newRow["equivalentAmount"] = 0;
            newRow["recordID"] = "5076f7ca-38cb-49b3-a958-ff688ffe9de3";
            newRow["IssueDate"] = DateTime.ParseExact("2030-05-28", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            newRow["Viddoc"] = 16;
            newRow["Person_id"] = 1;
            newRow["id_familyUD"] = 5;

            dt.Rows.Add(newRow);
        }
    }
}
