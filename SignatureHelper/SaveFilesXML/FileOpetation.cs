﻿using SignatureHelper;
using System;
using SignatureHelper.SignatureCrypto;
using SignatureHelper.FolderFileOperation;
using SignatureHelper.SaveFilesXML.PathNameFormat;
using SignatureHelper.DataBaseOperations;
using SignatureHelper.ClassesFromXSD.Assigment_Fact;
using SignatureHelper.ClassesFromXSD.lonelyChildren;
using SignatureHelper.ClassesFromXSD.lonelyChildren.xsd;

namespace SignatureHelper.SaveFilesXML
{
    public class FileOpetation
    {
        bool _detachableSignature;     

        public bool DetachableSignature
        {
            get { return _detachableSignature; }
            set { _detachableSignature = value; }
        }

        public FileOpetation(FileNameXML fileNameXML)
        {
            _fileNameXML = fileNameXML;
        }

        FileNameXML _fileNameXML;
        public FileOpetation(bool detachableSignature, FileNameXML fileNameXML)
        {
            _detachableSignature = detachableSignature;
            _fileNameXML = fileNameXML;
        }
       

        public void SaveXMLOnPath(LonelyChildren.data dataPackage, string fileName, string pathToSave, FolderNameEnum folderName)
        {
            try
            {
                var xmlOperation = new XMLOperation();

                xmlOperation.SaveXmlFile(dataPackage, fileName, pathToSave, folderName);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                Console.WriteLine(ex.InnerException.Message);                
            }
        }

        public void SaveXMLOnPath(data dataPackage, string fileName, string pathToSave, FolderNameEnum folderName )
        {
            try
            {
                var xmlOperation = new XMLOperation();                

                xmlOperation.SaveXmlFile(dataPackage, fileName, pathToSave, folderName);              
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //throw new Exception(ex.InnerException.Message);
            }
        }

    }    
}
