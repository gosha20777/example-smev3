﻿using Dapper;
using SignatureHelper.ClassesFromXSD.DownloadProtocol;
using SignatureHelper.SignatureCrypto;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using static FTP;

namespace SignatureHelper.DataBaseOperations
{
    public enum TypeResponse : int
    {
        UNKNOW = -1,
        EGISSO = 1,
        SMEV3 = 2
    }

    public class InsertProtocol
    {
        public int InsertFileReports(string reportsPath)
        {
            var reports = Directory.GetFiles(reportsPath);
            var countProcessedProtocols = 0;
            foreach (var fileReport in reports)
            {
                if (fileReport.Contains("10.05.I"))
                {
                    var xmlFile = System.IO.File.ReadAllText(fileReport);
                    SaveReportsXML<FTP.response>(xmlFile);
                    countProcessedProtocols++;
                }

                if (fileReport.Contains("10.06.S") || fileReport.Contains("10.21.S"))
                {
                    var xmlFile = System.IO.File.ReadAllText(fileReport);
                    SaveReportsXML<FTP.response>(xmlFile);
                    countProcessedProtocols++;
                }
            }

            return  countProcessedProtocols;

            //MessageBox.Show($"Загрузка протоколов xml успешно закончена в количестве {i} шт", "XML с протоколами",
            //MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        public void SaveReportsXML<T>(string fileXML) where T : class, new()
        {
            var xdoc = XDocument.Parse(fileXML);
            var serializeConfig = new XMLOperation();
            SavePackageResult<T>(serializeConfig.DeSerializeXMLstring<T>(fileXML));

            SaveErrorResult<T>(serializeConfig.DeSerializeXMLstring<T>(fileXML));

            SaveProtocolXML(xdoc);
        }

        private void SaveErrorResult<T>(T response) where T : class, new()
        {
            string insertPackageResult =
            @"INSERT INTO [dbo].[egisso_ProtocolFA_mena]
                ([packageID], [recordID], [recordOK])
            VALUES (@packageID, @recordID, @recordOK)";

            using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
            {
                conn.Open();

                var packResult = GetResponseType<T>(response);

                var packageID = Guid.Parse(packResult.packageID);

                foreach (var errorRecord in packResult.errorRecords)
                {
                    var recID = Guid.Parse(errorRecord.recID);
                    var recordOK = errorRecord.recordOK;

                    var result = conn.Execute(insertPackageResult, new
                    {
                        packageID,
                        recordID = recID,
                        recordOK,

                    });

                    InsertErrorInfo(errorRecord.messages, recID);
                }
            }
        }

        public void ChangeProtocolStatus(string requestGUID, TypeResponse typeProtocol, string rejectionReasonCode = "", string rejectionReasonDescription = "")
        {
            string query = @"update [dbo].[SMEV3QueueRequests] 
                set ParseProtocolSuccess = 1, TypeProtocol= @TypeProtocol, RejectionReasonCode = @RejectionReasonCode, 
                RejectionReasonDescription = @RejectionReasonDescription   where RequestGUID = @RequestGUID ";

            using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
            {
                conn.Open();

                var protocol = new
                {
                    RequestGUID = requestGUID,
                    TypeProtocol = typeProtocol,
                    RejectionReasonCode = rejectionReasonCode,
                    RejectionReasonDescription = rejectionReasonDescription
                };

                var result = conn.Execute(query, protocol);
            }
        }

        public void InsertErrorInfo(dynamic errorMessages, Guid recID)
        {
            if (errorMessages != null)
            {
                string query =
            @"INSERT INTO [dbo].[egisso_ErrRecord_mena]
                (recordID, [messageType], [ruleCode], [report])
            values(@recordID, @messageType, @ruleCode, @report)";

                using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
                {
                    conn.Open();

                    foreach (var message in errorMessages)
                    {
                        var ruleCode = message.ruleCode;
                        var messageType = message.messageType;

                        var report = message.report;

                        var res = conn.Execute(query, new
                        {
                            recordID = recID,
                            messageType,
                            ruleCode,
                            report
                        });

                    }
                }
            }
        }


        private void SaveProtocolPackageResult<T>(T response) where T : class, new()
        {
            string query = @"INSERT INTO [dbo].[EGISSO_packageResult]([packageID], [packageStatus], 
                                    [receivingTime],[startProcessTime],
                                    [finishProcessTime], [recordNum],
                                    [packageType], [recordNumSuccess] ) 
                VALUES (@packageID, @packageStatus, 
                @receivingTime, 
                @startProcessTime,
                @finishProcessTime,
                @recordNum, @packageType, @recordNumSuccess)";

            var packageResult = GetResponseType<T>(response);

            using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
            {
                conn.Open();

                var protocol = new
                {
                    packageResult.packageID,
                    packageResult.packageStatus,
                    packageResult.receivingTime,
                    packageResult.startProcessTime,
                    packageResult.finishProcessTime,
                    packageResult.recordNum,
                    packageResult.packageType,
                    recordNumSuccess = packageResult.recordNumSuccess ?? 0
                };

                var result = conn.Execute(query, protocol);
            }
        }

        private dynamic GetResponseType<T>(T response) where T : class
        {

            if (response is FTP.response)
            {
                return (response as FTP.response).protocol.Item as tPackageResult1;
            }
            else
            {
                throw new Exception("Нет требуемого типа ProtocolLMSZ или ProtocolAssigmentFact для сохранения в бд xml");
            }
        }

        /// <summary>
        /// Сохранение данных элеманта в пространсве имен ns8
        /// </summary>
        /// <param name="xdoc"></param>
        private void SavePackageResult<T>(T response) where T : class, new()
        {
            SaveProtocolPackageResult<T>(response);
        }

        private T GetProtocol<T>(string xml) where T : class
        {
            var doc = new XmlDocument();
            var xmlOperration = new XMLOperation();

            doc.LoadXml(xml);

            return ProtocolFromXML<T>(doc);
        }


        private dynamic ProtocolFromXML<T>(XmlDocument doc) where T : class
        {
            var xmlOperation = new XMLOperation();
            XmlNodeList elemList = doc.GetElementsByTagName("packageResult", @"urn://egisso-ru/types/package-protocol/1.0.3");
            if (elemList[0] != null)
            {
                elemList = doc.GetElementsByTagName("protocol", @"urn://egisso-ru/types/package-protocol/1.0.3");
                return xmlOperation.DeSerializeXMLstring<T>(elemList[0].InnerText);
            }

            elemList = doc.GetElementsByTagName("messageResult", @"urn://egisso-ru/types/package-protocol/1.0.3");
            if (elemList[0] != null)
            {
                elemList = doc.GetElementsByTagName("protocol", @"urn://egisso-ru/types/package-protocol/1.0.3");
                return xmlOperation.DeSerializeXMLstring<FTP.tPackageProtocolMessageResult>(elemList[0].InnerText);
            }

            return null;
        }

        /// <summary>
        /// Сохранение всех протоколов и информации которая хранится в них БД в таблицу EGISSO_receivedXML
        /// </summary>
        /// <param name="xdoc"></param>
        public void SaveProtocolXML(XDocument xdoc, int source = 0)
        {
            string query = @"INSERT INTO [dbo].[EGISSO_receivedXML]([receivedXML], [Source]) 
                VALUES (@receivedXML, @Source)";

            using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
            {
                conn.Open();

                var result = conn.Execute(query, new
                {
                    receivedXML = xdoc,
                    Source = source
                });
            }
        }

        //private void SaveProtocolMessageResult<T>(T response) where T : class, new()
        //{
        //    throw new NotImplementedException();
        //}

        public void SaveReportsMessageResult<T>(string fileXML, string envelopeGUID) where T : class
        {
            var xdoc = XDocument.Parse(fileXML);
            var serializeConfig = new XMLOperation();
            SaveMessageResult(serializeConfig.DeSerializeXMLstring<FTP.response>(fileXML), envelopeGUID);
            SaveProtocolXML(xdoc);
        }

        private void SaveMessageResult(FTP.response response, string envelopeGUID)
        {
            var errorMesssage = response.protocol.Item as FTP.tPackageProtocolMessageResult;

            string query = @"INSERT INTO [dbo].[EGISSO_messageResult]([envelopeGUID], [receivingTime], [processingTime]) 
                VALUES (@envelopeGUID, @receivingTime, @processingTime)
                ; SELECT CAST(SCOPE_IDENTITY() as int)";

            using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
            {
                conn.Open();

                var result = conn.QueryAsync<int>(query, new
                {
                    envelopeGUID,
                    errorMesssage.receivingTime,
                    errorMesssage.processingTime
                }).GetAwaiter().GetResult().Single();

                foreach (var error in errorMesssage.errors)
                {
                    SaveErrmessageResult(error, envelopeGUID, result);
                }
            }
        }

        private void SaveErrmessageResult(FTP.tError response, string envelopeGUID, int idEgissoMessageResult)
        {
            string query = @"INSERT INTO [dbo].[EGISSO_ErrmessageResult]([id_egisso_messageResult], [code], [message], [time], [envelopeGUID] ) 
                VALUES (@id_egisso_messageResult, @code, @message, @time, @envelopeGUID)";

            using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
            {
                conn.Open();

                var result = conn.Execute(query, new
                {
                    envelopeGUID,
                    id_egisso_messageResult = idEgissoMessageResult,
                    response.code,
                    response.message,
                    response.time
                });
            }
        }
    }
}
