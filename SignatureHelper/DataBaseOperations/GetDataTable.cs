﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SignatureHelper.FolderFileOperation;

namespace SignatureHelper.DataBaseOperations
{
    /// <summary>
    /// Получение таблицы DataTable в памяти путем выполнения sql запроса с параметрами
    /// </summary>
    public static class GetDataTable
    {
        /// <summary>
        /// Реализация строки соединения с запросом и передачей параметров для запроса.
        /// </summary>
        /// <param name="query">Sql запрос</param>
        /// <param name="parameters">Параметры для Sql запроса</param>
        /// <returns>Запрос для дальнейшей его обработки</returns>
        public static DataTable GetQuery(string connString, string query, List<SqlParameter> parameters = null)
        {
            using (var con = new SqlConnection(connString))
            {
                con.Open();
                var cmd = new SqlCommand(query);
                {
                    cmd.Connection = con;
                    SqlParameter param = new SqlParameter();
                    //задаем имя параметра
                    if (parameters != null)
                    {
                        cmd.Parameters.AddRange(parameters.ToArray());
                    }

                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    sqlDataAdapter.Fill(dataTable);
                    sqlDataAdapter.Dispose();

                    return dataTable;
                }
            }
        }


        public static string ConnectionString()
        {
            string GetSQLServerName = PathSettings.ServerName();
            string GetNAMEBD = PathSettings.BaseName();
            return string.Format("server={0};database={1};Integrated Security=SSPI", GetSQLServerName, GetNAMEBD);
        }
    }
}