﻿
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using CryptoPro.Sharpei;
using CryptoPro.Sharpei.Xml;
using SignatureHelper.ClassesFromXSD.Assigment_Fact;
using SignatureHelper.ClassesFromXSD.lonelyChildren.xsd;
using SignatureHelper.ClassesFromXSD.SMEV;
using SignatureHelper.FolderFileOperation;

namespace SignatureHelper.SignatureCrypto
{

    public class XMLOperation
    {

        public dynamic GetMailFromXML(string xml)
        {
            var packageResult = EgissoFact(xml);
            if (packageResult != null)
            {
                return packageResult;
            }

            var messageResult = EgissoChildNeedHouseRight(xml);
            if (messageResult != null)
            {
                return messageResult;
            }

            return null;
        }

        public data EgissoFact(string xml)
        {
            var doc = new XmlDocument();
            var xmlOperration = new XMLOperation();
            data data = null;

            doc.LoadXml(xml);

            XmlNodeList elemList = doc.GetElementsByTagName("data", @"urn://egisso-ru/msg/10.06.S/1.0.4");

            if (elemList[0] != null)
            {
                data = xmlOperration.DeSerializeXMLstring<data>(elemList[0].OuterXml);
            }
            return data;
        }

        public LonelyChildren.data EgissoChildNeedHouseRight(string xml)
        {
            var doc = new XmlDocument();
            var xmlOperration = new XMLOperation();
            LonelyChildren.data data = null;

            doc.LoadXml(xml);

            XmlNodeList elemList = doc.GetElementsByTagName("data", @"urn://egisso-ru/msg/10.21.S/1.0.1");

            if (elemList[0] != null)
            {
                data = xmlOperration.DeSerializeXMLstring<LonelyChildren.data>(elemList[0].OuterXml);
            }
            return data;
        }



        public dynamic GetProtocolFromXML(string xml)
        {
            var packageResult = ResponseEGISSOFromXML(xml, "packageResult", "urn://egisso-ru/types/package-protocol/1.0.3");
            if (packageResult != null)
            {
                return packageResult;
            }


            var messageResult = ResponseEGISSOFromXML(xml, "messageResult", "urn://egisso-ru/types/package-protocol/1.0.3");
            if (messageResult != null)
            {
                return messageResult;
            }


            var smevProtocol = SMEVProtocolFromXML(xml);
            if (smevProtocol != null)
            {
                return smevProtocol;
            }

            return null;
        }

        public FTP.response ResponseEGISSOFromXML(string xml, string xmlElement, string nameSpace)
        {
            var doc = new XmlDocument();
            var xmlOperration = new XMLOperation();
            FTP.response protocol = null;

            doc.LoadXml(xml);

            XmlNodeList elemList = doc.GetElementsByTagName(xmlElement, nameSpace);

            if (elemList[0] != null)
            {
                elemList = doc.GetElementsByTagName("response", @"urn://egisso-ru/msg/10.10.I/1.0.3");
                protocol = xmlOperration.DeSerializeXMLstring<FTP.response>(elemList[0].OuterXml);
            }
            return protocol;
        }



        public Response SMEVProtocolFromXML(string xml)
        {
            var doc = new XmlDocument();
            var xmlOperration = new XMLOperation();
            Response protocol = null;

            doc.LoadXml(xml);

            XmlNodeList elemList = doc.GetElementsByTagName("Response", @"urn://x-artefacts-smev-gov-ru/services/message-exchange/types/1.1");

            if (elemList[0] != null)
            {
                protocol = xmlOperration.DeSerializeXMLstring<Response>(elemList[0].OuterXml);
            }
            return protocol;
        }

        public T SMEVProtocolFromXML<T>(string xml) where T : class
        {
            var doc = new XmlDocument();
            var xmlOperration = new XMLOperation();
            T protocol = null;

            doc.LoadXml(xml);

            XmlNodeList elemList = doc.GetElementsByTagName("Response", @"urn://x-artefacts-smev-gov-ru/services/message-exchange/types/1.1");

            if (elemList[0] != null)
            {
                protocol = xmlOperration.DeSerializeXMLstring<T>(elemList[0].OuterXml);
            }
            return protocol;
        }



        public void SaveXmlFile(object objectForCreateXML, string fileName, string PathFolderForSave,
            FolderNameEnum operation = FolderNameEnum.Create)
        {
            XmlDocument xml = new XmlDocument();

            string filePathXml;
            string pathForFolder;

            //Сохранение на \\mailserv\Public\19depExch\xml\
            //appSetting скажет какой берем путь из настроек
            pathForFolder = ConcatPathAndOpiration(PathFolderForSave, operation);

            filePathXml = $"{pathForFolder}{fileName}.xml";

            SaveXMLFiles(pathForFolder, filePathXml, objectForCreateXML);


        }

        public void SaveXMLFiles(string pathForFolder, string filePathXml, object objectForCreateXML)
        {
            if (File.Exists(filePathXml))
            {
                File.Delete(filePathXml);
            }

            File.AppendAllText(filePathXml, BeautifulViewXML(SerializeObject(objectForCreateXML)));
        }

        public void CreateFolderForXML(string folderName)
        {
            Directory.CreateDirectory(folderName);
        }

        public string ConcatPathAndOpiration(string path, FolderNameEnum operation)
        {

            return $@"{path}{DateTime.Today.ToString("dd-MM-yy")}\{operation}\";
        }



        public XmlDocument Serialize(Type type, object obj, XmlRootAttribute root, Int32 indexFileName = 1)
        {
            XmlSerializer xSer = (root == null) ? new XmlSerializer(type) : new XmlSerializer(type, root);

            StringWriter sw = new StringWriter();
            XmlTextWriter xWriter = new XmlTextWriter(sw);

            xSer.Serialize(xWriter, obj);

            xWriter.Flush();
            sw.Flush();

            XmlDocument xDoc = new XmlDocument();
            xDoc.PreserveWhitespace = true;

            xDoc.LoadXml(sw.GetStringBuilder().ToString());

            return xDoc;
        }

        //Создаем на основе объекта текстовую переменную с текстом в формате xml
        public string SerializeObject<T>(T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        public string SerializeToStringUtf16<T>(T data)
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));
            // Using a MemoryStream to store the serialized string as a byte array, 
            // which is "encoding-agnostic"
            using (MemoryStream ms = new MemoryStream())
            // Few options here, but remember to use a signature that allows you to 
            // specify the encoding  
            using (XmlTextWriter tw = new XmlTextWriter(ms, Encoding.Unicode))
            {
                tw.Formatting = Formatting.Indented;
                ser.Serialize(tw, data);
                // Now we get the serialized data as a string in the desired encoding
                return Encoding.Unicode.GetString(ms.ToArray());
            }
        }


        public void Serialize<T>(string path, T type)
        {
            var serializer = new XmlSerializer(type.GetType());

            using (var writer = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(writer, type);
            }
        }

        public T DeSerialize<T>(string path) where T : class
        {
            T type;
            var serializer = new XmlSerializer(typeof(T));
            using (var reader = XmlReader.Create(path))
            {
                type = serializer.Deserialize(reader) as T;
            }
            return type;
        }

        public T DeSerializeXMLstring<T>(string fileXML) where T : class
        {
            T type;
            var serializer = new XmlSerializer(typeof(T));
            using (var reader = XmlReader.Create(new System.IO.StringReader(fileXML)))
            {
                type = serializer.Deserialize(reader) as T;
            }
            return type;
        }


        public XmlDocument SerializeXMLToDocument(Type type, object obj, XmlRootAttribute root)
        {
            XmlSerializer xSer = (root == null) ? new XmlSerializer(type) : new XmlSerializer(type, root);

            StringWriter sw = new StringWriter();
            XmlTextWriter xWriter = new XmlTextWriter(sw);

            xSer.Serialize(xWriter, obj);

            xWriter.Flush();
            sw.Flush();

            XmlDocument xDoc = new XmlDocument();
            xDoc.PreserveWhitespace = true;
            xDoc.LoadXml(sw.GetStringBuilder().ToString());

            return xDoc;
        }
        public object DeserializeXMLToObejct(XmlDocument xDoc, Type type, XmlRootAttribute root)
        {
            StringReader sr = new StringReader(xDoc.OuterXml);
            XmlSerializer xs = (root == null) ? new XmlSerializer(type) : new XmlSerializer(type, root);
            return xs.Deserialize(sr);
        }


        public string FormattingXML<T>(T toSerialize)
        {
            return BeautifulViewXML(SerializeObject(toSerialize));
        }

        public string GetXMLDocument()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("path to your file");
            return BeautifulViewXML(doc.InnerXml);
        }

        //Форматируем текст в текстовой переменной для удобного отображения xml структуры.
        public String BeautifulViewXML(String XML)
        {
            String Result = "";

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
            XmlDocument document = new XmlDocument();

            try
            {
                // Load the XmlDocument with the XML.
                document.LoadXml(XML);

                var declarations = document.ChildNodes.OfType<XmlNode>()
                .Where(x => x.NodeType == XmlNodeType.XmlDeclaration)
                .ToList();

                declarations.ForEach(x => document.RemoveChild(x));

                writer.Formatting = Formatting.Indented;

                // Write the XML into a formatting XmlTextWriter
                document.WriteContentTo(writer);
                writer.Flush();
                mStream.Flush();

                // Have to rewind the MemoryStream in order to read
                // its contents.
                mStream.Position = 0;

                // Read MemoryStream contents into a StreamReader.
                StreamReader sReader = new StreamReader(mStream);

                // Extract the text from the StreamReader.
                String FormattedXML = sReader.ReadToEnd();

                Result = FormattedXML;
            }
            catch (XmlException)
            {
            }

            mStream.Close();
            writer.Close();

            return Result;
        }

        public string GetXMLProtocol<T>(T xml, string packageID)
        {
            var fileOperation = new XMLOperation();

            return fileOperation.FormattingXML(xml);

        }



        public string GetFormatName(string fileName, int i)
        {

            string fileIndex = "";

            ///аналог replicate
            fileIndex = String.Format("{0:000}", i);
            return fileName + fileIndex;
        }


        public void CreateSignFileInMemory(string filePath, string fileName, string serrialNumber)
        {
            //Сохранение на локальном пути компьютера
            var pathForFolder = ConcatPathAndOpiration(filePath, FolderNameEnum.Create);

            var fileNameXML = filePath + fileName + ".xml";

            var byteSign = GetDetachedSign(File.ReadAllBytes(fileNameXML), serrialNumber);

            var fileNameSign = fileNameXML + ".p7s";

            //if (removeOriginal) File.Delete(fullName);

            //return Path.GetFileName(fileSignName);

            File.WriteAllText(fileNameSign, byteSign);
        }

        //public string GetSignP7S(string filePath, string serrialNumber)
        //{
        //    return  GetDetachedSign(File.ReadAllBytes(fileNameXML), serrialNumber);
        //}


        public void SignFileOnPath(string pathAndFileName, string serrialNumber)
        {
            //Сохранение на локальном пути компьютера


            var fileNameXML = pathAndFileName;

            var byteSign = GetDetachedSign(File.ReadAllBytes(fileNameXML), serrialNumber);

            var fileNameSign = fileNameXML + ".p7s";

            //if (removeOriginal) File.Delete(fullName);

            //return Path.GetFileName(fileSignName);

            File.WriteAllText(fileNameSign, byteSign);
        }

        public string GetDetachedSign(byte[] fileDataToSign, string certSerialNumber)
        {
            var signPKS7 = GetCMS(fileDataToSign, certSerialNumber, true);
            return Convert.ToBase64String(signPKS7);
        }

        private byte[] GetCMS(byte[] data, string serialNumber, bool isDetached)
        {
            if (data == null)
                throw new ArgumentNullException("data", "Отсутствуют данные для подписывания");

            //  Создаем объект ContentInfo по сообщению.
            //  Это необходимо для создания объекта SignedCms.
            ContentInfo contentInfo = new ContentInfo(data);

            // Создаем объект SignedCms по только что созданному
            // объекту ContentInfo.
            SignedCms signedCms = new SignedCms(contentInfo, true);

            // Определяем подписывающего, объектом CmsSigner.
            var signer = new CmsSigner(clientCertificate(serialNumber));

            // Подписываем CMS/PKCS #7 сообение.
            signedCms.ComputeSignature(signer, false);

            //Проверка отсоединенной подписи
            if (!VerifySign(signedCms.Encode(), clientCertificate(serialNumber), data))
            {
                throw new CryptographicException();
            }

            // Кодируем CMS/PKCS #7 подпись сообщения.
            var signedbytes = signedCms.Encode();
            return signedbytes;
        }

        public void CheckSignatureInUsb(string serialNumber)
        {
            try
            {
                var xmlOperation = new XMLOperation();
                xmlOperation.clientCertificate(serialNumber);

                //  Создаем объект ContentInfo по сообщению.
                //  Это необходимо для создания объекта SignedCms.

                var data = Encoding.ASCII.GetBytes("<test><\test>");


                ContentInfo contentInfo = new ContentInfo(data);

                // Создаем объект SignedCms по только что созданному
                // объекту ContentInfo.
                SignedCms signedCms = new SignedCms(contentInfo, true);

                // Определяем подписывающего, объектом CmsSigner.
                var signer = new CmsSigner(clientCertificate(serialNumber));

                // Подписываем CMS/PKCS #7 сообение.
                signedCms.ComputeSignature(signer, false);

                //Проверка отсоединенной подписи
                if (!VerifySign(signedCms.Encode(), clientCertificate(serialNumber), data))
                {
                    throw new CryptographicException();
                }

                // Кодируем CMS/PKCS #7 подпись сообщения.
                var signedbytes = signedCms.Encode();

            }
            catch (Exception ex )
            {
                throw new Exception(ex.Message, ex);
            }

        }

        public X509Certificate2 clientCertificate(string serialNumber)
        {
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);

            X509Certificate2Collection coll = store.Certificates.Find(X509FindType.FindBySerialNumber, serialNumber, true);

            if (coll == null || coll.Count == 0) throw new Exception("Сертификат с указанным серийным номером отсутствует в локальном хранилище сертификатов!");
            else return coll[0];
        }


        public static bool VerifySign(byte[] signature, X509Certificate2 certificate, byte[] fileDataToSign)
        {
            if (signature == null)
                throw new ArgumentNullException("signature");
            if (certificate == null)
                throw new ArgumentNullException("certificate");


            //  Создаем объект ContentInfo по сообщению.
            //  Это необходимо для создания объекта SignedCms.
            ContentInfo contentInfo = new ContentInfo(fileDataToSign);

            // Создаем объект SignedCms по только что созданному
            // объекту ContentInfo.
            SignedCms signedCms = new SignedCms(contentInfo, true);

            // Определяем подписывающего, объектом CmsSigner.
            var signer = new CmsSigner(certificate);

            // Подписываем CMS/PKCS #7 сообение.
            signedCms.ComputeSignature(signer);

            signedCms.Decode(signature);

            // verify it
            try
            {
                signedCms.CheckSignature(true);
                return true;
            }
            catch (CryptographicException)
            {
                return false;
            }
        }

        // Подписывание XML документа
        public XmlElement SignedData(XmlDocument data, string uri, string serrialNumber)
        {
            SignedXml sXml = new SignedXml(data);

            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);

            var xmlOperation = new XMLOperation();
            var x509Sert = xmlOperation.clientCertificate(serrialNumber);

            var prov = (Gost3410_2012_256CryptoServiceProvider)x509Sert.PrivateKey;

            sXml.SigningKey = prov;

            KeyInfo keyInfo = new KeyInfo();
            keyInfo.AddClause(new KeyInfoX509Data(x509Sert));
            sXml.KeyInfo = keyInfo;

            Reference refer = new Reference();
            refer.Uri = "#" + uri;
            refer.DigestMethod = CPSignedXml.XmlDsigGost3411_2012_256Url;

            //refer.AddTransform(new XmlDsigEnvelopedSignatureTransform(true));
            refer.AddTransform(new XmlDsigExcC14NTransform());
            refer.AddTransform(new XmlDsigSmevTransform());

            sXml.AddReference(refer);

            sXml.SignedInfo.CanonicalizationMethod = new XmlDsigExcC14NTransform().Algorithm;

            sXml.SignedInfo.SignatureMethod = CPSignedXml.XmlDsigGost3410_2012_256Url;

            sXml.ComputeSignature();
            return sXml.Signature.GetXml();
        }

        ////"Создание отсоединенной подписи с указанным сертификатом для переданного файла данных в потоковом режиме." +
        ////@"<br>Параметры:<br>fileDataName - имя файла для обработки, находящегося в директории \\vckpws4\IISFilesCrypt" +
        ////@"<br>certSerialNumber - серийный номер сертификата, используемого для подписания файла данных" +
        ////@"<br>Метод возвращает имя файла, содержащего отсоединенную подпись, размещенного в директории \\vckpws4\IISFilesCrypt")]
        public string GetDetachedStreamSign(string fileDataName, string certSerialNumber, bool removeOriginal)
        {
            //var workingDir = @"C:\";
            string fullName = fileDataName;
            string fileSignName = new StreamSign().CreateSign(fullName, certSerialNumber);

            if (removeOriginal) File.Delete(fullName);

            return Path.GetFileName(fileSignName);
        }


        // [WebMethod(Description = "Создание архива заданного типа из файлов по переданным именам, работа с путями к файлам." +
        //@"<br>Параметры:<br>files - массив имен файлов для обработки, находящихся в директории \\vckpws4\IISFilesCrypt" +
        //@"<br>type - тип архива, который требуется создать (перечисление ArchiveType)" +
        //@"<br>Метод возвращает имя созданного архива, размещенного в директории \\vckpws4\IISFilesCrypt")]
        public string GetArchiveStr(string[] files, ArchiveType type, string workingDir)
        {
            if (files == null || files.Length == 0) return null;

            //var workingDir = @"D:\IISFilesCrypt\";

            string archPath = workingDir + "Archive_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
            string fullNames = String.Empty;
            foreach (string f in files) fullNames += " \"" + workingDir + f + "\"";

            try
            {
                Process pr = new Process();
                pr.StartInfo.FileName = "WinRAR.exe";
                pr.StartInfo.Arguments = @"a -af" + type.ToString().ToLower() + " -y -ep " + archPath + fullNames;
                pr.StartInfo.WorkingDirectory = @"C:\Program Files\WinRAR\";
                pr.Start();
                pr.WaitForExit();

                if (pr.ExitCode != 0) throw new Exception("WinRAR process exit code = " + pr.ExitCode);
            }
            catch (Exception ex)
            {
                throw new Exception("Error while executing WinRAR process", ex);
            }
            finally
            {
                //foreach (string f in files) File.Delete(workingDir + f);
            }
            return Path.GetFileName(archPath) + "." + type.ToString().ToLower();
        }

        public enum ArchiveType
        {//проверить в случае добавления новых типов, т.к. winRar может их не воспринимать,
         //тип архива должен совпадать с расширением архивов указанного типа
            ZIP,
            RAR
        }

        public struct FileData
        {
            public string name;
            public byte[] content;
        }

        public struct CertData
        {
            public string validFrom;
            public string validTo;
            public string subject;
        }
    }
}
