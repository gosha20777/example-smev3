﻿using CryptoPro.Sharpei;
using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace SignatureHelper.SignatureCrypto
{
    public class StreamSign
    {

        public enum ArchiveType
        {//проверить в случае добавления новых типов, т.к. winRar может их не воспринимать,
         //тип архива должен совпадать с расширением архивов указанного типа
            ZIP,
            RAR
        }

        public struct FileData
        {
            public string name;
            public byte[] content;
        }

        public string CreateSign(string fileDataPath, string certSerialNum)
        {
            bool detached = true;

            string fileSignPath = fileDataPath + ".sig";

            X509Certificate2 signer = GetSignerCert(certSerialNum);

            FileStream inFile = File.Open(fileDataPath, FileMode.Open);
            FileStream outFile = File.Create(fileSignPath);

            StreamSignHelper cms = new StreamSignHelper();

            try
            {
                cms.Encode(signer, inFile, outFile, detached);
            }
            catch (Exception)
            {
                outFile.Close();
                File.Delete(fileSignPath);
                outFile = null;
                throw;
            }
            finally
            {
                inFile.Close();
                if (outFile != null)
                    outFile.Close();
            }

            return fileSignPath;
        }

        static X509Certificate2 GetSignerCert(string serialNumber)
        {
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.OpenExistingOnly);

            X509Certificate2Collection coll = store.Certificates.Find(X509FindType.FindBySerialNumber, serialNumber, true);

            store.Close();

            if (coll == null || coll.Count == 0) throw new Exception("Сертификат с указанным серийным номером отсутствует в локальном хранилище сертификатов!");
            else return coll[0];
        }
    }

    class StreamSignHelper
    {
        // Поток, используемый в callback функции
        private FileStream m_callbackFile;

        // Размер блока кодирования
        private const int BlockSize = 1024000;

        // Потоковая функция кодирования
        private Boolean StreamOutputCallback(IntPtr pvArg, IntPtr pbData,
            int cbData, Boolean fFinal)
        {
            // Возможна 0 длина, например при fFinal = true
            // и detached
            if (cbData == 0)
                return true;
            // Запись переданных байт в конец файла.
            Byte[] bytes = new Byte[cbData];
            Marshal.Copy(pbData, bytes, 0, cbData);
            m_callbackFile.Write(bytes, 0, cbData);
            return true;
        }

        // Кодирование CMS в поточном режиме.
        public void Encode(X509Certificate2 cert, FileStream inFile,
            FileStream outFile, bool detached)
        {
            CMSWin32.CMSG_SIGNER_ENCODE_INFO SignerInfo;
            CMSWin32.CMSG_SIGNED_ENCODE_INFO SignedInfo;
            CMSWin32.CMSG_STREAM_INFO StreamInfo;
            CMSWin32.CERT_CONTEXT[] CertContexts = null;
            CMSWin32.BLOB[] CertBlobs;

            X509Chain chain = null;
            X509ChainElement[] chainElements = null;
            X509Certificate2[] certs = null;
            AsymmetricAlgorithm key = null;
            ICspAsymmetricAlgorithm ikey = null;
            GCHandle gchandle = new GCHandle();

            IntPtr hProv = IntPtr.Zero;
            IntPtr SignerInfoPtr = IntPtr.Zero;
            IntPtr CertBlobsPtr = IntPtr.Zero;
            IntPtr hMsg = IntPtr.Zero;
            IntPtr pbPtr = IntPtr.Zero;
            Byte[] pbData;
            long dwFileSize;
            long dwRemaining;
            int dwSize;
            Boolean bResult = false;

            try
            {
                // Размер файла.
                dwFileSize = inFile.Length;
                // Блок кодирования
                pbData = new byte[BlockSize];

                // поток закодированных данных
                m_callbackFile = outFile;

                // Строим цепочку сертификатов.
                chain = new X509Chain();
                chain.Build(cert);
                chainElements = new X509ChainElement[
                    chain.ChainElements.Count];
                chain.ChainElements.CopyTo(chainElements, 0);

                // Сворачиваем цепочку в массив
                certs = new X509Certificate2[chainElements.Length - 1/* !! */];
                for (int i = 0; i < chainElements.Length - 1 /* !! */; i++)
                {
                    certs[i] = chainElements[i].Certificate;
                }

                // Получаем Win32 контексты сертификатов
                CertContexts = new CMSWin32.CERT_CONTEXT[certs.Length];
                for (int i = 0; i < certs.Length; i++)
                {
                    CertContexts[i] = (CMSWin32.CERT_CONTEXT)
                        Marshal.PtrToStructure(certs[i].Handle,
                        typeof(CMSWin32.CERT_CONTEXT));
                }

                // Получаем все сертификаты в виде BLOB:");
                CertBlobs = new CMSWin32.BLOB[CertContexts.Length];
                for (int i = 0; i < CertContexts.Length; i++)
                {
                    CertBlobs[i].cbData = CertContexts[i].cbCertEncoded;
                    CertBlobs[i].pbData = CertContexts[i].pbCertEncoded;
                }

                // Получаем ключ по сертификату");
                key = certs[0].PrivateKey;

                // Получаем CSP для ключа");
                ikey = key as ICspAsymmetricAlgorithm;


                if (ikey == null)
                {
                    throw new CryptographicException(
                        "Private key dosn't support " +
                        "ICspAsymmetricAlgorithm interface");
                }

                // Открываем контекст ключа:");
                bResult = CMSWin32.CryptAcquireContext(
                    ref hProv, ikey.CspKeyContainerInfo.KeyContainerName,
                    ikey.CspKeyContainerInfo.ProviderName,
                    ikey.CspKeyContainerInfo.ProviderType, 0);
                if (!bResult)
                {
                    throw new CryptographicException(
                        "CryptAcquireContext error #" +
                        Marshal.GetLastWin32Error().ToString(),
                        new Win32Exception(Marshal.GetLastWin32Error()));
                }

                // Заполняем структуру SignerInfo");
                SignerInfo = new CMSWin32.CMSG_SIGNER_ENCODE_INFO();
                SignerInfo.cbSize = Marshal.SizeOf(SignerInfo);
                SignerInfo.pCertInfo = CertContexts[0].pCertInfo;
                SignerInfo.hCryptProvOrhNCryptKey = hProv;
                SignerInfo.dwKeySpec = (int)ikey.CspKeyContainerInfo.KeyNumber;
                // Для алгоритма ключа ГОСТ выставляем алгоритм хеширования ГОСТ.
                if (key is Gost3410)
                    SignerInfo.HashAlgorithm.pszObjId = CMSWin32.szOID_CP_GOST_R3411;
                else // для всех остальных SHA1
                    SignerInfo.HashAlgorithm.pszObjId = CMSWin32.szOID_OIWSEC_sha1;

                // Заполняем структуру SignedInfo");
                SignedInfo = new CMSWin32.CMSG_SIGNED_ENCODE_INFO();
                SignedInfo.cbSize = Marshal.SizeOf(SignedInfo);

                SignedInfo.cSigners = 1;
                SignerInfoPtr = Marshal.AllocHGlobal(
                    Marshal.SizeOf(SignerInfo));
                Marshal.StructureToPtr(SignerInfo, SignerInfoPtr, false);
                SignedInfo.rgSigners = SignerInfoPtr;

                SignedInfo.cCertEncoded = CertBlobs.Length;
                CertBlobsPtr = Marshal.AllocHGlobal(
                    Marshal.SizeOf(CertBlobs[0]) * CertBlobs.Length);
                for (int i = 0; i < CertBlobs.Length; i++)
                {
                    Marshal.StructureToPtr(CertBlobs[i], new IntPtr(
                        CertBlobsPtr.ToInt64() +
                        (Marshal.SizeOf(CertBlobs[i]) * i)), false);
                }
                SignedInfo.rgCertEncoded = CertBlobsPtr;

                // Заполняем структуру StreamInfo");
                StreamInfo = new CMSWin32.CMSG_STREAM_INFO();
                if (dwFileSize > Int32.MaxValue)
                    StreamInfo.cbContent = -1; // Используем BER
                else
                    StreamInfo.cbContent = (int)dwFileSize; // Используем DER
                StreamInfo.pfnStreamOutput = new
                    CMSWin32.StreamOutputCallbackDelegate(StreamOutputCallback);

                // При необходимости устанавливаем flag отсоединенной подписи
                Int32 flags = 0;
                if (detached)
                    flags |= CMSWin32.CMSG_DETACHED_FLAG;

                // Открываем сообщение для кодирования");
                hMsg = CMSWin32.CryptMsgOpenToEncode(
                    CMSWin32.X509_ASN_ENCODING | CMSWin32.PKCS_7_ASN_ENCODING,
                    flags, CMSWin32.CMSG_SIGNED, ref SignedInfo, null,
                    ref StreamInfo);
                if (hMsg.Equals(IntPtr.Zero))
                {
                    throw new CryptographicException(
                        "CryptMsgOpenToEncode error #" +
                        Marshal.GetLastWin32Error().ToString(),
                        new Win32Exception(Marshal.GetLastWin32Error()));
                }

                // Поблочная обработка сообщения");
                gchandle = GCHandle.Alloc(pbData, GCHandleType.Pinned);
                pbPtr = gchandle.AddrOfPinnedObject();
                dwRemaining = dwFileSize;
                if (dwFileSize < BlockSize)
                    dwSize = (int)dwFileSize;
                else
                    dwSize = BlockSize;
                while (dwRemaining > 0)
                {
                    inFile.Read(pbData, 0, dwSize);
                    // Вызываем обработку блока
                    bResult = CMSWin32.CryptMsgUpdate(hMsg, pbPtr, dwSize,
                        (dwRemaining <= dwSize) ? true : false);
                    if (!bResult)
                    {
                        throw new CryptographicException(
                            "CryptMsgUpdate error #" +
                            Marshal.GetLastWin32Error().ToString(),
                            new Win32Exception(Marshal.GetLastWin32Error()));
                    }

                    // Переходим к следующему блоку
                    dwRemaining -= dwSize;
                    if (dwRemaining < dwSize)
                    {
                        dwSize = (int)dwRemaining;
                    }
                }
            }
            finally
            {
                // Чистим
                if (gchandle.IsAllocated) gchandle.Free();
                if (!CertBlobsPtr.Equals(IntPtr.Zero)) Marshal.FreeHGlobal(CertBlobsPtr);
                if (!SignerInfoPtr.Equals(IntPtr.Zero)) Marshal.FreeHGlobal(SignerInfoPtr);
                if (!hProv.Equals(IntPtr.Zero)) CMSWin32.CryptReleaseContext(hProv, 0);
                if (!hMsg.Equals(IntPtr.Zero)) CMSWin32.CryptMsgClose(hMsg);
            }
        }
    }
}

