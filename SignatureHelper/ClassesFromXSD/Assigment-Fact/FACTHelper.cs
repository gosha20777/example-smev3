﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignatureHelper.ClassesFromXSD.Assigment_Fact
{
    public class FACTHelper
    {
        public  tPersonInfoIdentityDoc GetPassportType(DataRow infoAssigmentFact)
        {
            //Данные паспорта
            var passportRFInfo = new  PassportRFType()
            {
                Series = infoAssigmentFact["Series"].ToString(),
                Number = infoAssigmentFact["Number"].ToString(),
                IssueDate = Convert.ToDateTime(infoAssigmentFact["IssueDate"].ToString()),
                Issuer = infoAssigmentFact["Issuer"].ToString(),

            };

            //Данные свидетельства о рождении
            var passportSovietInfo = new  SovietPassportType()
            {
                Series = infoAssigmentFact["Series"].ToString(),
                Number = infoAssigmentFact["Number"].ToString(),
                IssueDate = Convert.ToDateTime(infoAssigmentFact["IssueDate"].ToString()),
                Issuer = infoAssigmentFact["Issuer"].ToString()

            };


            var docINFO = new  tPersonInfoIdentityDoc()
            {

            };

            switch (infoAssigmentFact["Viddoc"])
            {
                case 2:
                case 16:
                    docINFO.ItemElementName =  ItemChoiceType.PassportRF;
                    docINFO.Item = passportRFInfo;
                    break;
                case 3:
                case 18:
                    docINFO.ItemElementName =  ItemChoiceType.BirthCertificate;
                    docINFO.Item = passportSovietInfo;
                    break;
                default:
                    ///Если документа нет, тогда требуется дописать код
                    return docINFO;

            }

            return docINFO;
        }

        public  tPersonInfo GetPesonIfo(DataRow infoAssigmentFact,  tPersonInfoIdentityDoc docINFO,  tPersonInfoAddress address)
        {

            var personInfo = new  tPersonInfo
            {
                SNILS = infoAssigmentFact["SNILS"].ToString(),
                FamilyName = infoAssigmentFact["FamilyName"].ToString(),
                FirstName = infoAssigmentFact["FirstName"].ToString(),
                Gender = infoAssigmentFact["Gender"].ToString().ToUpper().Equals("MALE") ?  GenderType.Male :  GenderType.Female,
                BirthDate = Convert.ToDateTime(infoAssigmentFact["BirthDate"]),
                Citizenship = infoAssigmentFact["Citizenship"].ToString(),
                //IdentityDoc = docINFO,
                //Address = address,
            };

            if (!string.IsNullOrWhiteSpace(infoAssigmentFact["Patronymic"].ToString()))
            {
                personInfo.Patronymic = infoAssigmentFact["Patronymic"].ToString();
            }


            return personInfo;
        }

        public object GetAmountForm(DataRow infoAssigmentFact)
        {
            switch (Convert.ToInt32(infoAssigmentFact["id_assignmentInfo"]))
            {
                ///monetaryForm
                case 1:
                    var monetaryForm = new  tFactAssignmentAssignmentInfoMonetaryForm()
                    {
                        amount = String.Format("{0:F2}", Convert.ToDecimal(infoAssigmentFact["amount_Monetary"])),

                    };
                    return monetaryForm;

                ///naturalForm
                case 2:
                    var naturalForm = new  tFactAssignmentAssignmentInfoNaturalForm()
                    {
                        amount = Convert.ToDecimal(infoAssigmentFact["amount"]),
                        content = infoAssigmentFact["content"].ToString(),
                        comment = infoAssigmentFact["comment"].ToString(),
                        equivalentAmount = 0,
                        ItemElementName =  ItemChoiceType1.measuryCode,

                        Item = infoAssigmentFact["measuryCode"].ToString(),

                    };
                    return naturalForm;

                ///exemptionForm
                case 3:
                    return 3;
                ///serviceForm
                case 4:
                    return 4;
                default:
                    Console.WriteLine("Default case");
                    return new Exception("Нет требуемого типа денежной формы!");
            }
        }
    }
}
