﻿namespace SignatureHelper.FolderFileOperation
{
    public enum FolderNameEnum
    {
        /// <summary>
        /// Создание фала XML
        /// </summary>
        Create,
        /// <summary>
        /// Изменине уже выгруженной записи 
        /// </summary>
        Change
    };

}
