﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EGISSO.Spinner
{
    public class WaitBar : CircularProgressBar.CircularProgressBar, IDisposable
    {
        Point _point;

        public WaitBar(Point point)
        {
            _point = point;
            Spinner = new CircularProgressBar.CircularProgressBar();
            InitializeComponent(Spinner);
        }

        public new void Dispose()
        {
            Spinner.Dispose();
            base.Dispose();
        }

        private CircularProgressBar.CircularProgressBar _spinner;

        public CircularProgressBar.CircularProgressBar Spinner
        {
            get { return _spinner; }
            set { _spinner = value; }
        }
        private void InitializeComponent(CircularProgressBar.CircularProgressBar circularProgressBar)
        {
            circularProgressBar.SuspendLayout();
            // 
            // circularProgressBar
            //  
            //Custom
            circularProgressBar.Style = ProgressBarStyle.Marquee;
            circularProgressBar.Visible = true;
            circularProgressBar.Minimum = 0;
            circularProgressBar.Maximum = 100;
            circularProgressBar.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            circularProgressBar.AnimationSpeed = 500;
            circularProgressBar.BackColor = System.Drawing.Color.Transparent;
            circularProgressBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 22, System.Drawing.FontStyle.Bold);
            circularProgressBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            circularProgressBar.InnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            circularProgressBar.InnerMargin = 2;
            circularProgressBar.InnerWidth = -1;
            circularProgressBar.MarqueeAnimationSpeed = 2000;
            circularProgressBar.Name = "circularProgressBar";
            circularProgressBar.OuterColor = System.Drawing.Color.Gray;
            circularProgressBar.OuterMargin = -25;
            circularProgressBar.OuterWidth = 26;
            circularProgressBar.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            circularProgressBar.ProgressWidth = 25;
            circularProgressBar.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            circularProgressBar.Size = new System.Drawing.Size(500, 320);
            circularProgressBar.StartAngle = 270;
            circularProgressBar.SubscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            circularProgressBar.SubscriptMargin = new System.Windows.Forms.Padding(10, -35, 0, 0);
            circularProgressBar.SubscriptText = "";
            circularProgressBar.SuperscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            circularProgressBar.SuperscriptMargin = new System.Windows.Forms.Padding(10, 35, 0, 0);
            circularProgressBar.SuperscriptText = "";
            circularProgressBar.TabIndex = 0;
            circularProgressBar.Text = "Идет обработка";
            circularProgressBar.TextMargin = new System.Windows.Forms.Padding(8, 8, 0, 0);
            circularProgressBar.Value = 70;
            circularProgressBar.Location = new Point() { X = _point.X - (int)(Size.Height / 1.3), Y = _point.Y - (Size.Width / 2) };
            circularProgressBar.ResumeLayout(false);
        }
    }
}
