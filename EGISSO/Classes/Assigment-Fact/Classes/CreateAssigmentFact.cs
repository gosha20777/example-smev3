﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Runtime.CompilerServices;
using SignatureHelper;
using System.Threading.Tasks;
using System.Threading;
using SignatureHelper.SignatureCrypto;
using SignatureHelper.FolderFileOperation;
using SignatureHelper.SaveFilesXML.PathNameFormat;
using SignatureHelper.DataBaseOperations;
using SignatureHelper.SaveFilesXML;
using SignatureHelper.ClassesFromXSD.Assigment_Fact;

[assembly: InternalsVisibleTo("UnitTestProject1")]
namespace EGISSO
{
    internal class CreateAssigmentFact
    {
        FileNameXML _fileNameXML = new FileNameXML();

        public int CreateAssigmentFactXml(bool detachableSignature)
        {
            var dtOperation = new DataBaseOperation();
            var dt = dtOperation.GetDataTableFromDB(SqlQuery.QueryAssigmentFact);

            ////Факты назначения
            ///
            ///Группировка по пакетам чтобы разбить файлы по районам, и отсортировать семьи в каждой группировке
            var groupsPackageGuid = dt.AsEnumerable()
                .OrderBy(r => r["districtID"])
                .ThenBy(t => t["packageID"])
                .ThenBy(r => r["id_familyUD"])
                .ThenBy(r => r["sam"])
                .GroupBy(g => g["packageID"]).ToList();

            int i = 0;

            var xmlOperation = new XMLOperation();
            var pathFolder = xmlOperation.ConcatPathAndOpiration(PathSettings.PathToMailservFACT(), FolderNameEnum.Create);

            var existFolder = Task.Factory.StartNew(() => xmlOperation.CreateFolderForXML(pathFolder));
            existFolder.Wait();

            groupsPackageGuid.AsParallel().ForAll(p =>
            {
                var index = Interlocked.Increment(ref i);
                FileNameXML _fileNameXML = new FileNameXML();
                var fileOperation = new FileOpetation(detachableSignature, _fileNameXML);

                //Пакет запроса данных Получателя МСЗ
                var packageList = new List<tFactAssignment2>();
                Guid packGuid = Guid.Empty;

                var creatingXML = Task.Factory.StartNew(() => GetXMLAssigmFact(p, packGuid, dt, packageList, _fileNameXML));
                creatingXML.Wait();

                var fileName = xmlOperation.GetFormatName($"{_fileNameXML.FileName}", index);

                fileOperation.SaveXMLOnPath(creatingXML.Result, fileName, PathSettings.PathToMailservFACT(), FolderNameEnum.Create);

                if (fileOperation.DetachableSignature)
                {
                    var path = xmlOperation.ConcatPathAndOpiration(PathSettings.PathToMailservFACT(), FolderNameEnum.Create);
                    xmlOperation.CreateSignFileInMemory(path, fileName, PathSettings.SerrialNumber());
                }

                dtOperation.InsertProtocolOnDB(creatingXML.Result, dtOperation);
            });

            return i;
        }

        public data GetXMLAssigmFact(IGrouping<object, DataRow> packageGuid, Guid packGuid, DataTable dt,
            List<tFactAssignment2> packageList, FileNameXML _fileNameXML)
        {
            foreach (var infoAssigmentFact in packageGuid)
            {
                //Вариант чтобы формировать один факт на сама всей семьи, а не на сама и на всех членов семьи и на каждого члена по факту.
                if ((int)infoAssigmentFact["sam"] != 1 && (bool)infoAssigmentFact["one_fact_family"])
                {
                    continue;
                }

                _fileNameXML.CodeOrganization = infoAssigmentFact["short_kod"].ToString();
                _fileNameXML.CodeFile = infoAssigmentFact["name_extension"].ToString();

                packGuid = (Guid)infoAssigmentFact["packageID"];

                var needsCriteria = new tFactAssignmentNeedsCriteria()
                {
                    usingSign = Convert.ToBoolean(infoAssigmentFact["usingSign"]),
                };

                /// Информация о людях
                var assignmentInfo = new tFactAssignmentAssignmentInfo()
                {
                    Item = GetAmountForm(infoAssigmentFact),
                };

                var assigmentFact = new tFactAssignment2()
                {
                    uuid = infoAssigmentFact["recordID"].ToString(),
                    oszCode = infoAssigmentFact["OSZCode"].ToString(),

                    lmszId = infoAssigmentFact["LMSZID"].ToString(),
                    categoryId = infoAssigmentFact["categoryID"].ToString(),
                    decisionDate = Convert.ToDateTime(infoAssigmentFact["decision_date"]),
                    dateStart = Convert.ToDateTime(infoAssigmentFact["dateStart"]),
                    needsCriteria = needsCriteria,
                    assignmentInfo = assignmentInfo

                };

                if (!string.IsNullOrWhiteSpace(infoAssigmentFact["lmszProviderCode"].ToString()))
                {
                    assigmentFact.lmszProviderCode = infoAssigmentFact["lmszProviderCode"].ToString();
                }

                if (infoAssigmentFact["dateFinish"] != DBNull.Value && infoAssigmentFact["dateFinish"] != null)
                {
                    //Чтобы в xml появился элемент dateFinish, dateFinishSpecified должен быть true
                    assigmentFact.dateFinish = Convert.ToDateTime(infoAssigmentFact["dateFinish"]);
                    assigmentFact.dateFinishSpecified = true;
                }

                //var t = new tFactModification()
                //{

                //};

                ///// Изменения факта назначения
                //if ((int)infoAssigmentFact["typeWork"] == 1)
                //{

                //    assigment lastChanging = Convert.ToDateTime(infoAssigmentFact["lastChanging"]);
                //    assigment previosID = infoAssigmentFact["baseID"].ToString();
                //}

                ///// Дата прекращение факта назначения
                //if ((int)infoAssigmentFact["typeWork"] == 2)
                //{
                //    var termination = new  tFactTermination()
                //    {
                //        uuid = infoAssigmentFact["ID"].ToString(),
                //        assignmentFactUuid = infoAssigmentFact["baseID"].ToString(),
                //        dateFinish = Convert.ToDateTime(infoAssigmentFact["dateFinish"].ToString()),
                //        lastChanging = Convert.ToDateTime(infoAssigmentFact["lastChanging"])

                //    };
                //}

                //Данные паспорта
                var passportRFInfo = new PassportRFType()
                {
                    Series = infoAssigmentFact["Series"].ToString(),
                    Number = infoAssigmentFact["Number"].ToString(),
                    IssueDate = Convert.ToDateTime(infoAssigmentFact["IssueDate"].ToString()),
                    Issuer = infoAssigmentFact["Issuer"].ToString(),

                };

                //Данные свидетельства о рождении
                var passportSovietInfo = new SovietPassportType()
                {
                    Series = infoAssigmentFact["Series"].ToString(),
                    Number = infoAssigmentFact["Number"].ToString(),
                    IssueDate = Convert.ToDateTime(infoAssigmentFact["IssueDate"].ToString()),
                    Issuer = infoAssigmentFact["Issuer"].ToString(),

                };

                //Тип паспорта
                var docINFO = GetPassportType(infoAssigmentFact);

                //адрес
                var addressInfo = new tAddress()
                {
                    Region = infoAssigmentFact["Region"].ToString(),
                    City = infoAssigmentFact["City"].ToString(),
                    District = infoAssigmentFact["District"].ToString(),
                    OKSMCode = infoAssigmentFact["OKSMCode"].ToString(),
                    PostIndex = infoAssigmentFact["PostIndex"].ToString(),
                    Apartment = infoAssigmentFact["Apartment"].ToString(),
                    Street = infoAssigmentFact["Street"].ToString(),
                    House = infoAssigmentFact["House"].ToString(),
                    Housing = infoAssigmentFact["Housing"].ToString(),

                };

                var address = new tPersonInfoAddress()
                {
                    PermanentResidence = addressInfo
                };

                //Группировака для вставки всех людей в блок данных reason_persons
                var groupsReasonPersons = dt.AsEnumerable().GroupBy(r => new
                {
                    ID = r.Field<int>("Person_id"),
                    IDCategory = r.Field<int>("id_familyUD")
                })
                    .Select(grp => grp.First()).Where(x => (x.Field<int>("id_familyUD") == (int)infoAssigmentFact["id_familyUD"]));

                var personInfoList = new List<tPersonInfo>();
                foreach (var item in groupsReasonPersons)
                {
                    if (groupsReasonPersons.Count() > 1)
                    {
                        personInfoList.Add(GetPesonIfo(item, docINFO, address));
                    }
                }

                if (groupsReasonPersons.Count() > 1)
                {
                    assigmentFact.reasonPersons = personInfoList.ToArray();
                }

                ///Вставка всех кто находится в семье. Они все получатели.
                assigmentFact.mszReceiver = GetPesonIfo(infoAssigmentFact, docINFO, address);

                packageList.Add(assigmentFact);
            }

            var packageElements = new tPackageElements()
            {
                fact = packageList.ToArray(),
            };



            var newPackage = new tPackage()
            {
                packageId = packGuid.ToString(),
                elements = packageElements,

            };


            var dataPackage = new data()
            {
                package = newPackage,
            };

            return dataPackage;
        }

        public void SaveXMLOnPath(data dataPackage, int i, bool detachableSignature)
        {
            try
            {
                var xmlOperation = new XMLOperation();
                var fileNameXML = new FileNameXML();
                var fileName = xmlOperation.GetFormatName($"{fileNameXML.FileName}", i);
                var dataBaseOperation = new DataBaseOperation();

                xmlOperation.SaveXmlFile(dataPackage, fileName, PathSettings.PathToMailservFACT(), FolderNameEnum.Create);

                if (detachableSignature)
                {
                    xmlOperation.CreateSignFileInMemory(xmlOperation.ConcatPathAndOpiration(PathSettings.PathToMailservFACT(), FolderNameEnum.Create), fileName, PathSettings.SerrialNumber());
                }

                var xmlReport = xmlOperation.GetXMLProtocol<data>(dataPackage, dataPackage.package.packageId);

                dataBaseOperation.InsertToBase(xmlReport, dataPackage.package.packageId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //throw new Exception(ex.InnerException.Message);
            }
        }


        static string workingDir = @"C:\prj\temp\13-06-19\Create\"; //ConfigurationManager.AppSettings["pathLocalPCFolder"];
        public string GetDetachedStreamSign(string fileDataName, string certSerialNumber, bool removeOriginal)
        {
            string fullName = workingDir + fileDataName;
            string fileSignName = new StreamSign().CreateSign(fullName, certSerialNumber);

            if (removeOriginal) File.Delete(fullName);

            return Path.GetFileName(fileSignName);
        }


        ///// <summary>
        //////Сохранение Сериализованного файла в БД
        ///// </summary>
        ///// <param name="xml"></param>
        //public void InsertToBase(FACT.data xml)
        //{
        //    string insertQuery = @"INSERT INTO [dbo].[EGISSO_PackageXML](packageID, XMLforSend) 
        //        VALUES (@packageID, @xml)";

        //    var fileOperation = new XMLOperation();

        //    var xmlFile = fileOperation.FormattingXML(xml);

        //    using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
        //    {
        //        conn.Open();

        //        var result = conn.Execute(insertQuery, new
        //        {
        //            xml.package.packageId,
        //            xml = xmlFile
        //        });

        //    }

        //}




        public tPersonInfoIdentityDoc GetPassportType(DataRow infoAssigmentFact)
        {
            //Данные паспорта
            var passportRFInfo = new PassportRFType()
            {
                Series = infoAssigmentFact["Series"].ToString(),
                Number = infoAssigmentFact["Number"].ToString(),
                IssueDate = Convert.ToDateTime(infoAssigmentFact["IssueDate"].ToString()),
                Issuer = infoAssigmentFact["Issuer"].ToString(),

            };

            //Данные свидетельства о рождении
            var passportSovietInfo = new SovietPassportType()
            {
                Series = infoAssigmentFact["Series"].ToString(),
                Number = infoAssigmentFact["Number"].ToString(),
                IssueDate = Convert.ToDateTime(infoAssigmentFact["IssueDate"].ToString()),
                Issuer = infoAssigmentFact["Issuer"].ToString()

            };


            var docINFO = new tPersonInfoIdentityDoc()
            {

            };

            switch (infoAssigmentFact["Viddoc"])
            {
                case 2:
                case 16:
                    docINFO.ItemElementName = ItemChoiceType.PassportRF;
                    docINFO.Item = passportRFInfo;
                    break;
                case 3:
                case 18:
                    docINFO.ItemElementName = ItemChoiceType.BirthCertificate;
                    docINFO.Item = passportSovietInfo;
                    break;
                default:
                    ///Если документа нет, тогда требуется дописать код
                    return docINFO;

            }

            return docINFO;
        }


        public tPersonInfo GetPesonIfo(DataRow infoAssigmentFact, tPersonInfoIdentityDoc docINFO, tPersonInfoAddress address)
        {

            var personInfo = new tPersonInfo
            {
                SNILS = infoAssigmentFact["SNILS"].ToString(),
                FamilyName = infoAssigmentFact["FamilyName"].ToString(),
                FirstName = infoAssigmentFact["FirstName"].ToString(),
                Gender = infoAssigmentFact["Gender"].ToString().ToUpper().Equals("MALE") ? GenderType.Male : GenderType.Female,
                BirthDate = Convert.ToDateTime(infoAssigmentFact["BirthDate"]),
                Citizenship = infoAssigmentFact["Citizenship"].ToString(),
                //IdentityDoc = docINFO,
                //Address = address,
            };

            if (!string.IsNullOrWhiteSpace(infoAssigmentFact["Patronymic"].ToString()))
            {
                personInfo.Patronymic = infoAssigmentFact["Patronymic"].ToString();
            }


            return personInfo;
        }

        public object GetAmountForm(DataRow infoAssigmentFact)
        {
            switch (Convert.ToInt32(infoAssigmentFact["id_assignmentInfo"]))
            {
                ///monetaryForm
                case 1:
                    var monetaryForm = new tFactAssignmentAssignmentInfoMonetaryForm()
                    {
                        amount = String.Format("{0:F2}", Convert.ToDecimal(infoAssigmentFact["amount_Monetary"])),

                    };
                    return monetaryForm;

                ///naturalForm
                case 2:
                    var naturalForm = new tFactAssignmentAssignmentInfoNaturalForm()
                    {
                        amount = Convert.ToDecimal(infoAssigmentFact["amount"]),
                        content = infoAssigmentFact["content"].ToString(),
                        comment = infoAssigmentFact["comment"].ToString(),
                        equivalentAmount = 0,
                        ItemElementName = ItemChoiceType1.measuryCode,

                        Item = infoAssigmentFact["measuryCode"].ToString(),

                    };
                    return naturalForm;

                ///exemptionForm
                case 3:
                    return 3;
                ///serviceForm
                case 4:
                    return 4;
                default:
                    Console.WriteLine("Default case");
                    return new Exception("Нет требуемого типа денежной формы!");
            }
        }


    }
}
