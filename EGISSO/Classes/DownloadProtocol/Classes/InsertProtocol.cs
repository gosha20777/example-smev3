﻿//using Dapper;
//using SignatureHelper.ProtocolLMSZ;
//using System;
//using System.Data.SqlClient;
//using System.IO;
//using System.Windows.Forms;
//using System.Xml.Linq;
//using SignatureHelper.SignatureCrypto;
//using SignatureHelper.DataBaseOperations;

//namespace EGISSO.xsd.DownloadProtocol.Classes
//{
//    public class InsertProtocol
//    {
//        public void InsertFileReports(string reportsPath)
//        {
            

//            var reports = Directory.GetFiles(reportsPath);
//            var i = 0;
//            foreach (var fileReport in reports)
//            {
//                if (fileReport.Contains("10.05.I"))
//                {
//                    SaveReportsXML<ProtocolLMSZ.response>(fileReport);
//                    i++;
//                }

//                if (fileReport.Contains("10.06.S") || fileReport.Contains("10.21.S"))
//                {
//                    SaveReportsXML<ProtocolAssigmentFact.response>(fileReport);
//                    i++;
//                }
//            }

//            MessageBox.Show($"Загрузка протоколов xml успешно закончена в количестве {i} шт", "XML с протоколами",
//            MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
//        }

//        private void SaveReportsXML<T>(string fileReport) where T : class, new() 
//        {
//            var xdoc = XDocument.Load(fileReport);
//            var serializeConfig = new XMLOperation.SerializeDeSerializeXML();
//            SavePackageResult<T>(serializeConfig.DeSerialize<T>(fileReport));

//            SaveErrorResult<T>(serializeConfig.DeSerialize<T>(fileReport));

//            SaveProtocolXML(xdoc);
//        }

//        /// <summary>
//        /// Сохранение всех протоколов и информации которая хранится в них
//        /// </summary>
//        /// <param name="fileInfo"></param>
//        public void SaveProtocolXML(XDocument xdoc)
//        {
//            SaveProtocol(xdoc);
//        }

//        private void SaveErrorResult<T>(T response) where T : class, new()
//        {
//            string insertPackageResult =
//            @"INSERT INTO [dbo].[egisso_ProtocolFA_mena]
//                ([packageID], [recordID], [recordOK])
//            VALUES (@packageID, @recordID, @recordOK)";

//            using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
//            {
//                conn.Open();

//                var packResult = GetResponseType<T>(response);

//                var packageID = Guid.Parse(packResult.packageID);

//                foreach (var errorRecord in packResult.errorRecords)
//                {
//                    var recID = Guid.Parse(errorRecord.recID);
//                    var recordOK = errorRecord.recordOK;

//                    var result = conn.Execute(insertPackageResult, new
//                    {
//                        packageID,
//                        recordID = recID,
//                        recordOK,

//                    });

//                    InsertErrorInfo(errorRecord.messages, recID);
//                }
//            }
//        }

//        public void InsertErrorInfo(dynamic errorMessages, Guid recID)
//        {
//            if (errorMessages != null)
//            {
//                string insertErrRecordMena =
//            @"INSERT INTO [dbo].[egisso_ErrRecord_mena]
//                (recordID, [messageType], [ruleCode], [report])
//            values(@recordID, @messageType, @ruleCode, @report)";

//                using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
//                {
//                    conn.Open();

//                    foreach (var message in errorMessages)
//                    {
//                        var ruleCode = message.ruleCode;
//                        var messageType = message.messageType;

//                        var report = message.report;

//                        var res = conn.Execute(insertErrRecordMena, new
//                        {
//                            recordID = recID,
//                            messageType,
//                            ruleCode,
//                            report
//                        });

//                    }
//                }
//            }
//        }


//        private void SaveProtocol<T> (T response) where T : class, new()
//        {
//            string insertPackageResult = @"INSERT INTO [dbo].[EGISSO_packageResult]([packageID], [packageStatus], 
//                                    [receivingTime],[startProcessTime],
//                                    [finishProcessTime], [recordNum],
//                                    [packageType], [recordNumSuccess] ) 
//                VALUES (@packageID, @packageStatus, 
//                @receivingTime, 
//                @startProcessTime,
//                @finishProcessTime,
//                @recordNum, @packageType, @recordNumSuccess)";

//            var packageResult = GetResponseType<T>(response);           

//            using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
//            {
//                conn.Open();                

//                var protocol = new
//                {
//                    packageResult.packageID,
//                    packageResult.packageStatus,
//                    packageResult.receivingTime,
//                    packageResult.startProcessTime,
//                    packageResult.finishProcessTime,
//                    packageResult.recordNum,
//                    packageResult.packageType,
//                    recordNumSuccess = packageResult.recordNumSuccess ?? DBNull.Value
//                };

//                var result = conn.Execute(insertPackageResult, protocol);
//            }
//        }

//        private dynamic GetResponseType<T>(T response) where T : class
//        {

//            if (response is ProtocolAssigmentFact.response)
//            {         
//                return (response as ProtocolAssigmentFact.response).protocol.Item;
//            }

//            if (response is ProtocolLMSZ.response)
//            {                              
//                return (response as ProtocolLMSZ.response).protocol.Item;
//            }
//            else 
//            {
//                throw new Exception("Нет требуемого типа ProtocolLMSZ или ProtocolAssigmentFact для сохранения в бд xml");
//            }
//        }

//        /// <summary>
//        /// Сохранение данных елеманта в пространсве имен ns8
//        /// </summary>
//        /// <param name="xdoc"></param>
//        private void SavePackageResult<T>(T response) where T : class, new()
//        {
//            SaveProtocol<T>(response);
//        }

//        /// <summary>
//        /// Сохранение файла протокала в БД
//        /// </summary>
//        /// <param name="xdoc"></param>
//        public void SaveProtocol(XDocument xdoc)
//        {
//            string insertQuery = @"INSERT INTO [dbo].[EGISSO_receivedXML]([receivedXML]) 
//                VALUES (@xdoc)";

//            using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
//            {
//                conn.Open();

//                var result = conn.Execute(insertQuery, new
//                {
//                    xdoc
//                });
//            }
//        }
//    }
//}
