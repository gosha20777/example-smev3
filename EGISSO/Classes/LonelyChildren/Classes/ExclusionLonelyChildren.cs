﻿using SignatureHelper.ClassesFromXSD.lonelyChildren.xsd;
using SignatureHelper.DataBaseOperations;
using SignatureHelper.FolderFileOperation;
using SignatureHelper.SaveFilesXML;
using SignatureHelper.SaveFilesXML.PathNameFormat;
using SignatureHelper.SignatureCrypto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EGISSO.Classes.lonelyChildren.Classes
{
    public class ExclusionLonelyChildren
    {
        public int CreateExclusionXML(bool detachableSignature)
        {
            var dtOperation = new DataBaseOperation();
            var dt = dtOperation.GetDataTableFromDB(SqlQuery.QueryExclusionLonelyChildren);

            ////Факты назначения
            ///
            ///Группировка по пакетам чтобы разбить файлы по районам, и отсортировать семьи в каждой группировке
            var groupsPackageGuid = dt.AsEnumerable()
                .GroupBy(g => g["packageID"]).ToList();

            int i = 0;

            var xmlOperation = new XMLOperation();
            var pathFolder = xmlOperation.ConcatPathAndOpiration(PathSettings.PathToMailservExclusionChildren(), FolderNameEnum.Create);

            var existFolder = Task.Factory.StartNew(() => xmlOperation.CreateFolderForXML(pathFolder));
            existFolder.Wait();

            groupsPackageGuid.AsParallel().ForAll(p =>
            {
                var index = Interlocked.Increment(ref i);
                FileNameXML _fileNameXML = new FileNameXML();
                var fileOperation = new FileOpetation(detachableSignature, _fileNameXML);

                //Пакет запроса данных для детей сирот
                var packageList = new List<LonelyChildren.tExclusionChildNeedHouse>();
                Guid packGuid = Guid.Empty;

                var creatingXML = Task.Factory.StartNew(() => GetExclusionXML(p, packGuid, packageList, _fileNameXML));
                creatingXML.Wait();

                var fileName = xmlOperation.GetFormatName($"{_fileNameXML.FileName}", index);

                fileOperation.SaveXMLOnPath(creatingXML.Result, fileName, PathSettings.PathToMailservExclusionChildren(), FolderNameEnum.Create);

                if (fileOperation.DetachableSignature)
                {
                    var path = xmlOperation.ConcatPathAndOpiration(PathSettings.PathToMailservExclusionChildren(), FolderNameEnum.Create);
                    xmlOperation.CreateSignFileInMemory(path, fileName, PathSettings.SerrialNumber());
                }

                dtOperation.InsertProtocolOnDB(creatingXML.Result, dtOperation);

            });

            return i;
        }

        public LonelyChildren.data GetExclusionXML(IGrouping<object, DataRow> packageGuid, Guid packGuid, List<LonelyChildren.tExclusionChildNeedHouse> packageList, FileNameXML fileNameXML)
        {
            foreach (var childs in packageGuid)
            {
                packGuid = (Guid)childs["packageID"];

                fileNameXML.CodeOrganization = childs["short_kod"].ToString();
                fileNameXML.CodeFile = childs["name_extension"].ToString();

                var exclusionChild = new LonelyChildren.tExclusionChildNeedHouse()
                {
                    uuid = childs["uuid"].ToString(),
                    reasonCode = childs["reasonCode"].ToString(),
                    codeOrg = childs["codeOrg"].ToString(),
                    act = new LonelyChildren.tSimpleDoc(),
                    lastChanging = Convert.ToDateTime(childs["lastChanging"]),
                    assignmentRightUuid = childs["assignmentRightUuid"].ToString(),
                    exclusionDate = Convert.ToDateTime(childs["exclusionDate"])
                };

                packageList.Add(exclusionChild);
            }

            var packageElements = new LonelyChildren.tPackageElements()
            {
                exclusionChildNeedHouseRight = packageList.ToArray(),

            };

            var newPackage = new LonelyChildren.tPackage()
            {
                packageId = packGuid.ToString(),
                elements = packageElements,

            };


            var dataPackage = new LonelyChildren.data()
            {
                package = newPackage,
            };

            return dataPackage;
        }


    }
}
