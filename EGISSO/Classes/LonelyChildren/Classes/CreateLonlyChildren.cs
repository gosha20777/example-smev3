﻿using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.Windows.Forms;
using System.Threading;
using SignatureHelper.FolderFileOperation;
using SignatureHelper.SignatureCrypto;
using SignatureHelper.DataBaseOperations;
using SignatureHelper.SaveFilesXML.PathNameFormat;
using SignatureHelper.SaveFilesXML;
using SignatureHelper.ClassesFromXSD.lonelyChildren.xsd;

namespace EGISSO.Classes.lonelyChildren.Classes
{
    public class CreatelonelyChildren
    {

        public int CreatelonelyChildrenXML(bool detachableSignature)
        {
            var dtOperation = new DataBaseOperation();
            var dt = dtOperation.GetDataTableFromDB(SqlQuery.QuerylonelyChildren);

            ////Факты назначения
            ///
            ///Группировка по пакетам чтобы разбить файлы по районам, и отсортировать семьи в каждой группировке
            var groupsPackageGuid = dt.AsEnumerable()
                .OrderBy(r => r["districtID"])
                .GroupBy(g => g["packageID"]).ToList();

            int i = 0;

            var xmlOperation = new XMLOperation();
            var pathFolder = xmlOperation.ConcatPathAndOpiration(PathSettings.PathToMailservInclusionChildren(), FolderNameEnum.Create);

            var existFolder = Task.Factory.StartNew(() => xmlOperation.CreateFolderForXML(pathFolder));
            existFolder.Wait();

            groupsPackageGuid.AsParallel().ForAll(p =>
            {
                var index = Interlocked.Increment(ref i);
                FileNameXML _fileNameXML = new FileNameXML();
                var fileOperation = new FileOpetation(detachableSignature, _fileNameXML);

                //Пакет запроса данных для детей сирот
                var packageList = new List<LonelyChildren.tChildNeedHouse>();
                Guid packGuid = Guid.Empty;

                var creatingXML = Task.Factory.StartNew(() => GetXMLlonelyChildren(p, packGuid, packageList, _fileNameXML));
                creatingXML.Wait();

                var fileName = xmlOperation.GetFormatName($"{_fileNameXML.FileName}", index);

                fileOperation.SaveXMLOnPath(creatingXML.Result, fileName, PathSettings.PathToMailservInclusionChildren(), FolderNameEnum.Create);

                if (fileOperation.DetachableSignature)
                {
                    var path = xmlOperation.ConcatPathAndOpiration(PathSettings.PathToMailservInclusionChildren(), FolderNameEnum.Create);
                    xmlOperation.CreateSignFileInMemory(path, fileName, PathSettings.SerrialNumber());
                }

                dtOperation.InsertProtocolOnDB(creatingXML.Result, dtOperation);


            });

            return i;
        }

        private LonelyChildren.data GetXMLlonelyChildren(IGrouping<object, DataRow> packageGuid, Guid packGuid, List<LonelyChildren.tChildNeedHouse> packageList, FileNameXML _fileNameXML)
        {

            foreach (var childs in packageGuid)
            {
                packGuid = (Guid)childs["packageID"];

                _fileNameXML.CodeOrganization = childs["short_kod"].ToString();
                _fileNameXML.CodeFile = childs["name_extension"].ToString();

                var childsNeedHouseRight = new LonelyChildren.tChildNeedHouse()
                {
                    uuid = childs["uuid"].ToString(),
                    codeMSZ = childs["codeMSZ"].ToString(),
                    categoryMSZ = childs["CategoryMSZ"].ToString(),
                    snils = childs["SNiLS"].ToString(),
                    lastName = childs["lastName"].ToString(),
                    firstName = childs["firstName"].ToString(),
                    birthDay = Convert.ToDateTime(childs["BirthDate"]),
                    registryDate = Convert.ToDateTime(childs["registryDate"]),
                    reasonCode = childs["reasonCode"].ToString(),
                    codeOrg = childs["codeOrg"].ToString(),
                    act = new LonelyChildren.tSimpleDoc(),
                    reasonDate = Convert.ToDateTime(childs["reasonDate"]),
                    npa18yo = new LonelyChildren.tDoc(),
                    lastChanging = Convert.ToDateTime(childs["lastChanging"]),
                };



                if (childs["middleName"] != DBNull.Value && childs["middleName"] != null && !string.IsNullOrEmpty(childs["middleName"].ToString()))
                {
                    childsNeedHouseRight.middleName = childs["middleName"].ToString();
                }

                packageList.Add(childsNeedHouseRight);
            }

            var packageElements = new LonelyChildren.tPackageElements()
            {
                childNeedHouseRight = packageList.ToArray(),
                //exclusionChildNeedHouseRight,
                //invalidationAssignmentRight
            };


            var newPackage = new LonelyChildren.tPackage()
            {
                packageId = packGuid.ToString(),
                elements = packageElements,

            };


            var dataPackage = new LonelyChildren.data()
            {
                package = newPackage,
            };

            return dataPackage;
        }
    }
}
