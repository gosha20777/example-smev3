﻿using System;
using System.IO;
using System.Windows.Forms;
using SignatureHelper.FolderFileOperation;
using SignatureHelper.SignatureCrypto;
using EGISSO.Classes.lonelyChildren.Classes;
using SignatureHelper.DataBaseOperations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using SignatureHelper.SaveFilesXML.PathNameFormat;
using System.Threading;
using SignatureHelper.ClassesFromXSD.lonelyChildren.xsd;
using SignatureHelper.SaveFilesXML;
using System.Collections.Generic;
using System.ComponentModel;
using EGISSO.Spinner;
using System.Drawing;

namespace EGISSO
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            signNumberTextBox.Text = PathSettings.SerrialNumber();
        }

        private void Form1_Load(object sender, System.EventArgs e)
        {
            // Start the BackgroundWorker.
            //backgroundWorker.RunWorkerAsync();
        }

        private void LocalMSZButton_Click(object sender, EventArgs e)
        {
            string pathFolder = PathSettings.PathToMailservlMSZ();

            ///ФОрмирование xml файла локальных мер 
            var objlocalMSZ = new CreatelocalMSZ(pathFolder);

            objlocalMSZ.GetLocalMSZ();

            System.Diagnostics.Process.Start(pathFolder);
        }

        private void ONMSZbutton_Click(object sender, EventArgs e)
        {
            string pathFolder = PathSettings.PathToMailservONMSZ();

            ///ФОрмирование xml файла ОНМСЗ
            var objONMSZ = new CreateONMSZ(pathFolder);

            objONMSZ.GetONMSZ();

            System.Diagnostics.Process.Start(pathFolder);
        }

        public Point CenterPosition()
        {
            return new Point((this.Width) / 2,
                          (this.Height) / 2);
        }

        private async void Factbutton_Click(object sender, EventArgs e)
        {
            using (WaitBar waitBar = new WaitBar(CenterPosition()))
            {
                try
                {
                    this.Controls.Add(waitBar.Spinner);
                    waitBar.Spinner.BringToFront();

                    //Подгодотка к формированию Фактов
                    var objAssigmentFact = new CreateAssigmentFact();

                    var xmlOperation = new XMLOperation();
                    if (CreateSignCheckBox.Checked)
                    {
                        await Task.Run(() => xmlOperation.CheckSignatureInUsb(PathSettings.SerrialNumber()));
                    }

                    //Формирование xml файла локальных мер    
                    var i = await Task.Run(() => objAssigmentFact.CreateAssigmentFactXml(CreateSignCheckBox.Checked));

                    MessageBox.Show($"XML файлы сформированы в количестве - {i} шт", "Формирования xml - Фактов назначений(Людей)",
                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    System.Diagnostics.Process.Start(PathSettings.PathToMailservFACT());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private async void IsertProtocolbutton_Click(object sender, EventArgs e)
        {
            using (WaitBar waitBar = new WaitBar(CenterPosition()))
            {
                try
                {
                    this.Controls.Add(waitBar.Spinner);
                    waitBar.Spinner.BringToFront();

                    var parseProtocol = new InsertProtocol();

                    var i = await Task.Run(() => parseProtocol.InsertFileReports(PathSettings.PathToMailservInsertReport()));

                    MessageBox.Show($"Загрузка протоколов xml успешно закончена в количестве {i} шт", "XML с протоколами",
                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void SaveSignNumberButton_Click(object sender, EventArgs e)
        {
            PathSettings.AddOrUpdateAppSettings("SignNumber", signNumberTextBox.Text);
        }

        private void PathForFileTextBox_Click(object sender, EventArgs e)
        {
            OpenFileDialog();
        }

        private async void SubscribeFileButton_Click(object sender, EventArgs e)
        {
            SubscribeFile(PathForFileTextBox.Text);
        }

        private async void SubscribeFile(string pathAndFileName)
        {
            using (WaitBar waitBar = new WaitBar(CenterPosition()))
            {
                try
                {
                    this.Controls.Add(waitBar.Spinner);
                    waitBar.Spinner.BringToFront();                   

                    var xmlOperation = new XMLOperation();

                    var taskPreparing = Task.Factory.StartNew(() => xmlOperation.CheckSignatureInUsb(PathSettings.SerrialNumber()));
                    await Task.WhenAll(taskPreparing);

                    if (!string.IsNullOrEmpty(pathAndFileName))
                    {
                        var taskSignature = Task.Factory.StartNew(() => xmlOperation.SignFileOnPath(pathAndFileName, PathSettings.SerrialNumber()));
                        await Task.WhenAll(taskSignature);

                        MessageBox.Show(pathAndFileName);
                        System.Diagnostics.Process.Start(Path.GetDirectoryName(pathAndFileName));
                    }
                    else
                    {
                        MessageBox.Show("Путь к файлу пуст!!!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void OpenFileDialog()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.DefaultExt = "xml";
            openFileDialog.Filter = "XML Files|*.xml";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;
            openFileDialog.FileName = PathSettings.PathForSubscribe();

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filename = openFileDialog.FileName;
                PathForFileTextBox.Text = filename;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LocalServiceForSMEV.ServiceClient client = new LocalServiceForSMEV.ServiceClient();

            client.Endpoint.Binding.SendTimeout = new TimeSpan(0, 60, 0);

            var req = new LocalServiceForSMEV.CommonRequest()
            {
                PathForSaveXML = $@"D:\SMEV3\FTP\302071\"
                //,AssigmentFact
            };
            LocalServiceForSMEV.CommonResponse rs = client.RSID30271(ref req);
        }

        private async void lonelyChild_Click(object sender, EventArgs e)
        {
            using (WaitBar waitBar = new WaitBar(CenterPosition()))
            {
                try
                {
                    var objlonelyChildren = new CreatelonelyChildren();
                    var xmlOperation = new XMLOperation();

                    this.Controls.Add(waitBar.Spinner);
                    waitBar.Spinner.BringToFront();

                    if (SignatureLonleyChildrenCb.Checked)
                    {
                        await Task.Run(() => xmlOperation.CheckSignatureInUsb(PathSettings.SerrialNumber()));
                    }

                    //Формирование xml файла локальных мер    
                    var i = await Task.Run(() => objlonelyChildren.CreatelonelyChildrenXML(SignatureLonleyChildrenCb.Checked));

                    MessageBox.Show($"XML файлы сформированы в количестве - {i} шт", "Формирование xml - Включение детей-сирот",
                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    System.Diagnostics.Process.Start(PathSettings.PathToMailservInclusionChildren());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private async void ExclusionLonelyChild_Click(object sender, EventArgs e)
        {
            var exclusionLonelyChildren = new ExclusionLonelyChildren();
            var xmlOperation = new XMLOperation();

            using (WaitBar waitBar = new WaitBar(CenterPosition()))
            {
                try
                {
                    this.Controls.Add(waitBar.Spinner);
                    waitBar.Spinner.BringToFront();

                    if (SignatureExclusionLonelyChild.Checked)
                    {
                        await Task.Factory.StartNew(() => xmlOperation.CheckSignatureInUsb(PathSettings.SerrialNumber()));
                    }

                    //Создать    
                    //Формирование xml файла локальных мер    
                    var i = await Task.Run(() => exclusionLonelyChildren.CreateExclusionXML(SignatureExclusionLonelyChild.Checked));

                    MessageBox.Show($"XML файлы сформированы в количестве - {i} шт", "Формирование xml - Исключение детей-сирот",
                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    System.Diagnostics.Process.Start(PathSettings.PathToMailservExclusionChildren());

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}



