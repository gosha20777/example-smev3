﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceModel.Channels;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using SignatureHelper.SignatureCrypto;
using SignatureHelper.FolderFileOperation;
using SMEV3QueueRequeries.Autofac.logger;
using Autofac;
using log4net;
using SMEV3QueueRequeries.DataBase.LoadingProtocol;

namespace SMEV3QueueRequeries
{
   public class Program
    {
        public static SqlConnection cn;      

        ILog logger;

        static void Main(string[] args)
        {
            Program program = new Program();
            IContainer container = new ProgramLogger().BuildContainer();
            program.logger = container.Resolve<ILog>();

            program.ExecuteService30271();

            program.ParseProtocols(program);

            program.SuccessRequest(program);
        }

        public void ParseProtocols(Program program)
        {
            var parseProtocol = new ParseProtocol(program.logger);
            var protocolsNeedParse = parseProtocol.ProtocolsForParsing();

            foreach (var protocol in protocolsNeedParse)
            {
                parseProtocol.ParseProtocolsFromXML(protocol.Response);
            }
        }

        public void SuccessRequest(Program program)
        {
            var parseProtocol = new ParseProtocol(program.logger);
            var successProtocols = parseProtocol.SuccessProtocols();

            foreach (var protocol in successProtocols)
            {
                parseProtocol.SetStatusExecution(protocol.id);
            }
        }



        public void ExecuteService30271()
        {
            // Привязка "потерянных" асинхронных запросов перед опросом очереди
            //SqlCommand cmd = new SqlCommand("INSERT LogsWSAsync SELECT SUBSTRING(RequestXML,326,36),RecordID,0 " +
            //    "FROM LogsWS WHERE RSID LIKE '3%' AND RType=0 AND DateAdded>CONVERT(datetime,'01.09.2018',104) " +
            //    "AND RecordID NOT IN (SELECT LogID FROM LogsWSAsync)", cn);
            //cmd.CommandType = CommandType.Text;
            //cmd.CommandTimeout = 120;

            //cn.Open();
            //cmd.ExecuteNonQuery();
            //cn.Close();

            SMEV3.Smev3ProdPortTypeClient client;

            SMEV3.MessageTypeSelector mts = new SMEV3.MessageTypeSelector()
            {
                Id = "SIGNED_BY_CALLER"
            };

            XmlElement callerSignature;
            do
            {
                #region Client Settings

                client = new SMEV3.Smev3ProdPortTypeClient();

                CustomBinding binding = new CustomBinding(client.Endpoint.Binding);
                binding.SendTimeout = new TimeSpan(0, 2, 0);

                binding.Elements.Find<TransportBindingElement>().MaxReceivedMessageSize = 8388608;

                binding.Elements.Remove<MtomMessageEncodingBindingElement>();
                binding.Elements.Insert(0, new SMEVMessageEncodingBindingElement());

                client.Endpoint.Binding = binding;

                #endregion


                mts.Timestamp = DateTime.Now;
                var xmlOperation = new XMLOperation();
                XmlDocument xd = xmlOperation.Serialize(typeof(SMEV3.MessageTypeSelector), mts, null);

                var opretationXml = new XMLOperation();
                var serrialNumber = opretationXml.clientCertificate(PathSettings.SerrialNumber());
                var taskSignData = Task.Factory.StartNew(() => opretationXml.SignedData(xd, mts.Id, serrialNumber.SerialNumber));
                taskSignData.Wait();

                callerSignature = taskSignData.Result;

                SMEV3.GetResponseResponseResponseMessage rm = client.GetResponse(mts, callerSignature);
                
                if (rm == null) break;

                SMEV3.AckTargetMessage atm = new SMEV3.AckTargetMessage()
                {
                    accepted = true,
                    Id = "SIGNED_BY_CALLER",
                    Value = rm.Response.MessageMetadata.MessageId
                };

                xd = xmlOperation.Serialize(typeof(SMEV3.AckTargetMessage), atm, null);
                callerSignature = opretationXml.SignedData(xd, atm.Id, serrialNumber.SerialNumber);

                #region Client Settings

                client = new SMEV3.Smev3ProdPortTypeClient();

                binding = new CustomBinding(client.Endpoint.Binding);
                binding.SendTimeout = new TimeSpan(0, 2, 0);

                binding.Elements.Remove<MtomMessageEncodingBindingElement>();
                binding.Elements.Insert(0, new SMEVMessageEncodingBindingElement());

                client.Endpoint.Binding = binding;

                #endregion

                try
                {
                    client.Ack(atm, callerSignature);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.GetBaseException().ToString());

                    Console.WriteLine(ex.GetBaseException().ToString());
                }

                try
                {
                    //new SMEV.ServiceClient().updateRequestPension(rm.Response.OriginalMessageId, "00", "Ответ получен");                
                 
                }
                catch (Exception ex)
                {
                    logger.Error(ex.GetBaseException().ToString());
                    Console.WriteLine(ex.GetBaseException().ToString());
                }
            }
            while (true);

        }
    }
}
