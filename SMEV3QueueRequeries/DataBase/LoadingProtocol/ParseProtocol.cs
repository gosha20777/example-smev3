﻿using Dapper;
using log4net;
using SignatureHelper.DataBaseOperations;
using SignatureHelper.SignatureCrypto;
using SMEV3QueueRequeries.SMEV3;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

using System.Xml.XPath;

namespace SMEV3QueueRequeries.DataBase.LoadingProtocol
{
    public class ParseProtocol
    {
        private ILog _log;
        public ParseProtocol(ILog log)
        {
            _log = log;
        }
        public IEnumerable<dynamic> ProtocolsForParsing()
        {
            try
            {
                string query =
                    @"Select 
                        * 
                    from 
                        [dbo].[SMEV3QueueRequests]
                    where
                        ParseProtocolSuccess = 0 
                        ";

                using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
                {
                    conn.Open();


                    return conn.Query(query);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        public IEnumerable<dynamic> SuccessProtocols()
        {
            try
            {
                string query =
                    @"
                    Select 
	                    distinct sl.id
                    from 
	                     SMEV3QueueRequests s 
                    inner join 
	                    SMEV3LoggingRequest sl on s.RequestGUID = sl.EnvelopeGuid
                    where 
	                    s.ParseProtocolSuccess = 1  and  sl.StatusExecution = 0 and s.StateId = 0
                        ";

                using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
                {
                    conn.Open();


                    return conn.Query(query);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        public void SetStatusExecution(int id)
        {
            try
            {
                string query = @"update [dbo].[SMEV3LoggingRequest] 
                   set StatusExecution = 1
                   where id = @id ";

                using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
                {
                    conn.Open();

                    var protocol = new
                    {
                        id
                    };

                    var result = conn.Execute(query, protocol);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw;
            }
        }

        public void ParseProtocolsFromXML(string protocolXml)
        {
            try
            {
                var xmlOperation = new XMLOperation();
                var protocol = xmlOperation.GetProtocolFromXML(protocolXml);
                var smevProtocol = xmlOperation.SMEVProtocolFromXML(protocolXml);

                var packageResult = xmlOperation.ResponseEGISSOFromXML(protocolXml, "packageResult", "urn://egisso-ru/types/package-protocol/1.0.3");
                if (packageResult != null)
                {
                    LoadProtocol(protocol as FTP.response, smevProtocol.OriginalMessageId);
                    return;
                }


                var messageResult = xmlOperation.ResponseEGISSOFromXML(protocolXml, "messageResult", "urn://egisso-ru/types/package-protocol/1.0.3");
                if (messageResult != null)
                {
                    LoadErrorProtocol(protocol as FTP.response, smevProtocol.OriginalMessageId);
                    return;
                }

                if (protocol is Response)
                {
                    LoadProtocol(smevProtocol.SenderProvidedResponseData.Items[0] as SenderProvidedResponseDataRequestRejected, smevProtocol.OriginalMessageId);
                    return;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        public void LoadProtocol(FTP.response ftpProtocol, string OriginalMessageId30271)
        {
            var xmlOpertation = new XMLOperation();
            var insertProtocol = new InsertProtocol();
            var protocol = xmlOpertation.SerializeObject<FTP.response>(ftpProtocol);
            insertProtocol.SaveReportsXML<FTP.response>(protocol);

            insertProtocol.ChangeProtocolStatus(OriginalMessageId30271, TypeResponse.EGISSO);
        }

        public void LoadErrorProtocol(FTP.response ftpProtocol, string originalMessageId)
        {
            var xmlOpertation = new XMLOperation();
            var insertProtocol = new InsertProtocol();       

            var protocol = xmlOpertation.SerializeObject<FTP.response>(ftpProtocol);
            insertProtocol.SaveReportsMessageResult<FTP.response>(protocol, originalMessageId);

            insertProtocol.ChangeProtocolStatus(originalMessageId, TypeResponse.EGISSO);
        }

        public void LoadProtocol(SenderProvidedResponseDataRequestRejected smevProtocol, string originalMessageId)
        {
            var insertProtocol = new InsertProtocol();
            insertProtocol.ChangeProtocolStatus(originalMessageId, TypeResponse.SMEV3,
                smevProtocol.RejectionReasonCode.ToString(), smevProtocol.RejectionReasonDescription);
        }

        //public dynamic GetProtocolFromXML(string xml)
        //{
        //    var packageResult = ResponseEGISSOFromXML(xml, "packageResult", "urn://egisso-ru/types/package-protocol/1.0.3");
        //    if (packageResult != null)
        //    {
        //        return packageResult;
        //    }


        //    var messageResult = ResponseEGISSOFromXML(xml, "messageResult", "urn://egisso-ru/types/package-protocol/1.0.3");
        //    if (messageResult != null)
        //    {
        //        return messageResult;
        //    }


        //    var smevProtocol = SMEVProtocolFromXML(xml);
        //    if (smevProtocol != null)
        //    {
        //        return smevProtocol;
        //    }

        //    return null;
        //}


        private string OriginalMessageId30271(string response)
        {
            var doc = new XmlDocument();
            var xmlOperration = new XMLOperation();
            string protocol = null;

            doc.LoadXml(response);

            XmlNodeList elemList = doc.GetElementsByTagName("OriginalMessageId", @"urn://x-artefacts-smev-gov-ru/services/message-exchange/types/1.1");

            if (elemList[0] != null)
            {
                return elemList[0].InnerText;
            }
            return protocol;
        }

        //private FTP.response ResponseEGISSOFromXML(string xml, string xmlElement, string nameSpace)
        //{
        //    var doc = new XmlDocument();
        //    var xmlOperration = new XMLOperation();
        //    FTP.response protocol = null;

        //    doc.LoadXml(xml);

        //    XmlNodeList elemList = doc.GetElementsByTagName(xmlElement, nameSpace);

        //    if (elemList[0] != null)
        //    {
        //        elemList = doc.GetElementsByTagName("response", @"urn://egisso-ru/msg/10.10.I/1.0.3");
        //        protocol = xmlOperration.DeSerializeXMLstring<FTP.response>(elemList[0].OuterXml);
        //    }
        //    return protocol;
        //}


        //public SMEV3.Response SMEVProtocolFromXML(string xml)
        //{
        //    var doc = new XmlDocument();
        //    var xmlOperration = new XMLOperation();
        //    SMEV3.Response protocol = null;

        //    doc.LoadXml(xml);

        //    XmlNodeList elemList = doc.GetElementsByTagName("Response", @"urn://x-artefacts-smev-gov-ru/services/message-exchange/types/1.1");

        //    if (elemList[0] != null)
        //    {
        //        //elemList = doc.GetElementsByTagName("Response", @"urn://x-artefacts-smev-gov-ru/services/message-exchange/types/1.1");


        //        protocol = xmlOperration.DeSerializeXMLstring<Response>(elemList[0].OuterXml);
        //    }
        //    return protocol;
        //}


        public XmlNodeList GetResponseProtocol(XmlDocument doc)
        {
            try
            {
                XmlNodeList responsesFact = doc.GetElementsByTagName("response", @"urn://egisso-ru/msg/10.10.I/1.0.3");
                if (responsesFact.Count != 0)
                {
                    return responsesFact;
                }

                XmlNodeList smevResponses = doc.GetElementsByTagName("response", @"urn://egisso-ru/msg/10.10.I/1.0.3");
                if (smevResponses.Count != 0)
                {
                    return smevResponses;
                }

                throw new Exception("Не известный протокол об ошибке!");
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }

        }

        public SenderProvidedResponseDataRequestRejected GetRequestRejected(SMEV3.Response protocol)
        {

            return (protocol.SenderProvidedResponseData.Items[0]) as SenderProvidedResponseDataRequestRejected;
        }


        //public void ParseProtocolsSMEV(string protocolXml)
        //{
        //    try
        //    {
        //        var smevProtocol = SMEVProtocolFromXML(protocolXml);

        //        GetProtocolFromXML(smevProtocol, smevProtocol.OriginalMessageId);

        //    }
        //    catch (Exception ex)
        //    {
        //        _log.Error(ex.GetBaseException().ToString());
        //        throw ex;
        //    }
        //}

        //public void GetProtocolFromXML(Response response, string originalMessageId)
        //{
        //    var data = response.SenderProvidedResponseData;
        //    for (int i = 0; i < response.SenderProvidedResponseData.ItemsElementName.Length; i++)
        //    {
        //        if (data.ItemsElementName[i].ToString().Contains("RequestRejected"))
        //        {
        //            LoadProtocol(data.Items[i] as SenderProvidedResponseDataRequestRejected, originalMessageId);
        //            return;
        //        }

        //        if (data.ItemsElementName[i].ToString().Contains("MessagePrimaryContent"))
        //        {
        //            LoadErrorProtocol(data.Items[i] as FTP.response, originalMessageId);
        //            return;
        //        }

        //        if (data.ItemsElementName[i].ToString().Contains("MessagePrimaryContent"))
        //        {
        //            LoadProtocol(data.Items[i] as FTP.response, originalMessageId);
        //            return;
        //        }
        //    }
        //}

        /// <summary>
        /// Не очень хороший метод, так как тип ответа от СМЭВ не всегда можно однозначно определить, из-за того что тип протокола 
        /// СМЭВ Будет иметь тип относительно неймспейса .... 
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public TypeResponse TypeProtocol(string xml )
        {
            var xmlOperation = new XMLOperation();
            var typeProtocol = xmlOperation.GetProtocolFromXML(xml);

            if (typeProtocol is FTP.response)
            {
                return TypeResponse.EGISSO;
            }

            if (typeProtocol is SMEV3.Response || typeProtocol is SignatureHelper.ClassesFromXSD.SMEV.Response)
            {
                return TypeResponse.SMEV3;
            }


            return TypeResponse.UNKNOW;
        }
    }


}
