﻿using Dapper;
using SignatureHelper.DataBaseOperations;
using log4net;
using SignatureHelper.SignatureCrypto;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;

namespace SMEV3QueueRequeries.DataBase
{
    public class MessageLogging
    {
        private ILog _log;
        public MessageLogging(ILog log)
        {
            _log = log;
        }
        
        public void SMEV3QueueRequestsResponse(string requestId, string request, string response, RespType resp, TypeResponse typeProtocol, string MessageId,
            string RejectionReasonCode = null, string RejectionReasonDescription = null)
        {
            try
            {
                string saveLogRequest =
                    @"INSERT INTO [dbo].[SMEV3QueueRequests]
                        ([Request], [Response],  [Error],  [RequestGuid], [SMEVResponseGuid], [RecordId], [StateId], [TypeProtocol]
                            , [RejectionReasonCode], [RejectionReasonDescription] )
                        VALUES (@Request, @Response, @Error,  @RequestGuid, @SMEVResponseGuid, @RecordId,  @StateId, @TypeProtocol
                            , @RejectionReasonCode, @RejectionReasonDescription)";

                using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
                {
                    conn.Open();           

                    var result = conn.Execute(saveLogRequest, new
                    {
                        Request = request,
                        Response = response,                      
                        Error = (byte)resp,                       
                        RequestGuid = requestId,
                        RecordId = (int)ParameterDirection.InputOutput,
                        StateId = (int)resp ,
                        SMEVResponseGuid = MessageId,
                        TypeProtocol = (int)typeProtocol,
                        RejectionReasonCode ,
                        RejectionReasonDescription  
                    });
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }
    }

    public enum RType : byte
    {
        REQ = 0,
        CHK = 1,
        CNL = 2,
        SYN = 3,
        ERR_REQ = 4,
        ERR_CHK = 5,
        ERR_CNL = 6,
        ERR_SYN = 7
    }
    public enum RespType : byte
    {
        DATA = 0,
        ERROR = 1,
        NO_DATA = 2,
        OTHER = 3, //custom user-defined error (LogsWS.RType 32bit)
        RETRY = 4 // Сервис поставщика недоступен, повторить инициализирующий запрос
    }
}
