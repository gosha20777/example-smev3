﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using SignatureHelper.SignatureCrypto;
using SignatureHelper.FolderFileOperation;
using SignatureHelper.SaveFilesXML.PathNameFormat;
using SignatureHelper.DataBaseOperations;
using SignatureHelper.ClassesFromXSD.oNMSZ;

namespace EGISSO
{
    class CreateONMSZ
    {
        private string pathFolder;

        public CreateONMSZ(string pathFolder)
        {
            this.pathFolder = pathFolder;
        }

        public void GetONMSZ()
        {
            var dt = GetSQLDataMSZ("0744");
            var fileNameXML = new FileNameXML();

            DataTable collapseTable = dt.Clone();            

            //локальные меры
            var groupsOrganization = dt.AsEnumerable().GroupBy(r => new { contractID = r.Field<Guid>("contractID") })
            .Select(grp => grp.First());

            //Администрация - организации которые находятся в определенном районе
            var groupsOrganizations = dt.AsEnumerable().GroupBy(r => new { contractID = r.Field<Guid>("contractID"), id_organization = r.Field<Int32>("id_organization") })
            .Select(grp => grp.First());

            //ID локльных мер которую предоставляют в администрации
            var groupsLocalMSZ = dt.AsEnumerable().GroupBy(r => new { id_organization = r.Field<Int32>("id_organization"), localMSZID = r.Field<Guid>("localMSZID") })
            .Select(grp => grp.First());

            //PointsAddress - адрес точек присутствия. Пока МФЦ
            var groupsPointsAddress = dt.AsEnumerable().GroupBy(r => new { ID = r.Field<int?>("id_MFC"), id_organization = r.Field<int?>("id_organization") })
            .Select(grp => grp.First());



            int i = 0;

            ///Заполнение локальномеры в одном файле она может быть только одна
            foreach (var rowOrganization in groupsOrganization)
            {
                i = ++i;

                fileNameXML.CodeOrganization = rowOrganization.Field<string>("short_kod").ToString();
                fileNameXML.CodeFile = rowOrganization.Field<string>("name_extension").ToString();

                var localtOrganization = new ONMSZ.tOrganization2()
                {
                    contractID = rowOrganization["contractID"].ToString(),
                    title = rowOrganization["title"].ToString(),
                    number = rowOrganization["number"].ToString(),
                    contractor = rowOrganization["contractor"].ToString(),
                    dateEnact = Convert.ToDateTime(rowOrganization["dateEnact"]),
                    dateExpiration = Convert.ToDateTime(rowOrganization["dateExpiration"])

                };


                var organizationsList = new List<ONMSZ.tOrganizationOrganization>();
                foreach (var itemOrganization in groupsOrganizations)
                {
                    var organizations = new ONMSZ.tOrganizationOrganization()
                    {
                        title = itemOrganization["title"].ToString(),
                        siteURL = itemOrganization["siteURL"].ToString(),
                        license = itemOrganization["license"].ToString(),
                        address = itemOrganization["address"].ToString(),
                        email = itemOrganization["email"].ToString()
                    };

                    var legalPerson = new ONMSZ.tOrganizationOrganizationLegalPerson()
                    {
                        INN = itemOrganization["legalINN"].ToString(),
                        KPP = itemOrganization["legalKPP"].ToString()
                    };



                    var individualBusinessman = new ONMSZ.tOrganizationOrganizationIndividualBusinessman()
                    {
                        INN = ""
                    };


                    var localMSZList = new List<string>();
                    foreach (var itemLocalMSZ in groupsLocalMSZ)
                    {

                        if (Convert.ToInt32(itemOrganization["id_organization"]) == Convert.ToInt32(itemLocalMSZ["id_organization"]))
                        {
                            //Добавление Идентификатор предоставляемой МСЗ
                            localMSZList.Add(itemLocalMSZ["localMSZID"].ToString());
                        }

                    }

                    //В блоке указываются сведения о точках присутствия указанной организации или ИП, в которые гражданин 
                    //может обратиться за получением соответствующей МСЗ(П)

                    var pointsAddressList = new List<ONMSZ.tOrganizationOrganizationPresencePoint>();
                    foreach (var itemPointsAddress in groupsPointsAddress)
                    {
                        if (itemPointsAddress["id_organization"].Equals(itemOrganization["id_organization"]))
                        {
                            var presencePoints = new ONMSZ.tOrganizationOrganizationPresencePoint()
                            {
                                title = itemPointsAddress["titleMFC"].ToString(),
                                town = itemPointsAddress["town"].ToString(),
                                street = itemPointsAddress["street"].ToString(),
                                house = itemPointsAddress["house"].ToString(),
                                housing = itemPointsAddress["housing"].ToString(),
                                building = itemPointsAddress["building"].ToString(),
                                note = itemPointsAddress["note"].ToString()
                            };

                            pointsAddressList.Add(presencePoints);
                        }

                    }

                    organizations.Item = legalPerson;

                    //organizations.Item = individualBusinessman;


                    organizations.localMSZ = localMSZList.ToArray();

                    organizations.presencePoints = pointsAddressList.ToArray();

                    //Элемент реестра организаций
                    organizationsList.Add(organizations);

                }

                localtOrganization.organizations = organizationsList.ToArray();

                //Пакет запроса
                var packageList = new List<ONMSZ.tOrganization2>();
                var newPackage = new ONMSZ.tPackage()
                {
                    packageID = Guid.NewGuid().ToString()

                };

                packageList.Add(localtOrganization);

                newPackage.elements = packageList.ToArray();

                //Запрос с пакеом и локальной мерой
                var request = new ONMSZ.request()
                {
                    package = newPackage
                };



                var fileOperation = new XMLOperation();
                

                fileOperation.SaveXmlFile(request, fileOperation.GetFormatName( $"{fileNameXML.FileName}",i),
                PathSettings.PathToMailservONMSZ(), FolderNameEnum.Create);

                var xmlReport = fileOperation.GetXMLProtocol<ONMSZ.request>(request, request.package.packageID);
            }

            //MessageBox.Show($"XML файлы сформированы в количестве - {i} шт", "Формирования xml - ОНМСЗ",
            //     MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }


        public DataTable GetSQLDataMSZ(string numberKMSZ)
        {
            var parameters = new List<SqlParameter>();

            SqlParameter param = new SqlParameter();

            //задаем значение параметра
            //param.ParameterName = "@NumberKMSZ";
            //param.Value = numberKMSZ;
            //parameters.Add(param);

            var dt = GetDataTable.GetQuery(GetDataTable.ConnectionString(), SqlQuery.QuerylocalONMSZ);

            return dt;
        }
    }
}
