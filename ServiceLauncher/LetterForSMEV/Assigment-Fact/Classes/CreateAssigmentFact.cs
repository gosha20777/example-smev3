﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Threading;
using SignatureHelper.SignatureCrypto;
using SignatureHelper.FolderFileOperation;
using SignatureHelper.SaveFilesXML.PathNameFormat;
using SignatureHelper.DataBaseOperations;
using SignatureHelper.SaveFilesXML;
using SignatureHelper.ClassesFromXSD.Assigment_Fact;
using log4net;


namespace ServiceLauncher
{
    public class CreateAssigmentFact
    {
        private readonly ILog log;
        public CreateAssigmentFact()
        {
            log = LogManager.GetLogger(typeof(CreateAssigmentFact));
        }

        public LauncherLocalService.CommonResponse SendFact(data AssigmFactXML)
        {
            try
            {
                LauncherLocalService.ServiceClient client = new LauncherLocalService.ServiceClient();

                client.Endpoint.Binding.SendTimeout = new TimeSpan(0, 60, 0);
                var opretationXml = new XMLOperation();
                var _assigmFactXML = opretationXml.FormattingXML(AssigmFactXML);

                var req = new LauncherLocalService.CommonRequest()
                {
                    MailEgisso = _assigmFactXML,
                    PathForSaveXML = $@"D:\SMEV3\FTP\302071\"
                };

                LauncherLocalService.CommonResponse rs = client.RSID30271(ref req);

                return rs;
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }

        }

        public int CreateAssigmentFactXml(bool detachableSignature)
        {
            try
            {
                var dtOperation = new DataBaseOperation();
                var dt = dtOperation.GetDataTableFromDB(SqlQuery.QueryAssigmentFact);

                ////Факты назначения
                ///
                ///Группировка по пакетам чтобы разбить файлы по районам, и отсортировать семьи в каждой группировке
                var groupsPackageGuid = dt.AsEnumerable()
                    .OrderBy(r => r["districtID"])
                    .ThenBy(t => t["packageID"])
                    .ThenBy(r => r["id_familyUD"])
                    .ThenBy(r => r["sam"])
                    .GroupBy(g => g["packageID"]).ToList();

                int i = 0;

                var xmlOperation = new XMLOperation();
                var pathFolder = xmlOperation.ConcatPathAndOpiration(PathSettings.PathToMailservFACT(), FolderNameEnum.Create);

                var existFolder = Task.Factory.StartNew(() => xmlOperation.CreateFolderForXML(pathFolder));
                existFolder.Wait();

                if (groupsPackageGuid.Count() < 1)
                {
                    log.Warn("Нет данных для отправки");
                    return 0;

                }

                groupsPackageGuid.AsParallel().ForAll(p =>
                {
                    var index = Interlocked.Increment(ref i);
                    FileNameXML _fileNameXML = new FileNameXML();
                    var fileOperation = new FileOpetation(detachableSignature, _fileNameXML);

                    //Пакет запроса данных Получателя МСЗ
                    var packageList = new List<tFactAssignment2>();
                    Guid packGuid = Guid.Empty;

                    var creatingXML = Task.Factory.StartNew(() => GetXMLAssigmFact(p, packGuid, dt, packageList, _fileNameXML));
                    creatingXML.Wait();
                   
                    var serviceExecuteTask = Task.Factory.StartNew(() => SendFact(creatingXML.Result));

                    serviceExecuteTask.Wait();
                });
                return i;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw ex;
            }
        }

        public data GetXMLAssigmFact(IGrouping<object, DataRow> packageGuid, Guid packGuid, DataTable dt,
            List<tFactAssignment2> packageList, FileNameXML _fileNameXML)
        {
            try
            {
                foreach (var infoAssigmentFact in packageGuid)
                {
                    //Вариант чтобы формировать один факт на сама всей семьи, а не на сама и на всех членов семьи и на каждого члена по факту.
                    if ((int)infoAssigmentFact["sam"] != 1 && (bool)infoAssigmentFact["one_fact_family"])
                    {
                        continue;
                    }

                    _fileNameXML.CodeOrganization = infoAssigmentFact["short_kod"].ToString();
                    _fileNameXML.CodeFile = infoAssigmentFact["name_extension"].ToString();

                    packGuid = (Guid)infoAssigmentFact["packageID"];

                    var needsCriteria = new tFactAssignmentNeedsCriteria()
                    {
                        usingSign = Convert.ToBoolean(infoAssigmentFact["usingSign"]),
                    };

                    var fACTHelper = new FACTHelper();

                    /// Информация о людях
                    var assignmentInfo = new tFactAssignmentAssignmentInfo()
                    {
                        Item = fACTHelper.GetAmountForm(infoAssigmentFact),
                    };

                    var assigmentFact = new tFactAssignment2()
                    {
                        uuid = infoAssigmentFact["recordID"].ToString(),
                        oszCode = infoAssigmentFact["OSZCode"].ToString(),

                        lmszId = infoAssigmentFact["LMSZID"].ToString(),
                        categoryId = infoAssigmentFact["categoryID"].ToString(),
                        decisionDate = Convert.ToDateTime(infoAssigmentFact["decision_date"]),
                        dateStart = Convert.ToDateTime(infoAssigmentFact["dateStart"]),
                        needsCriteria = needsCriteria,
                        assignmentInfo = assignmentInfo

                    };

                    if (!string.IsNullOrWhiteSpace(infoAssigmentFact["lmszProviderCode"].ToString()))
                    {
                        assigmentFact.lmszProviderCode = infoAssigmentFact["lmszProviderCode"].ToString();
                    }

                    if (infoAssigmentFact["dateFinish"] != DBNull.Value && infoAssigmentFact["dateFinish"] != null)
                    {
                        //Чтобы в xml появился элемент dateFinish, dateFinishSpecified должен быть true
                        assigmentFact.dateFinish = Convert.ToDateTime(infoAssigmentFact["dateFinish"]);
                        assigmentFact.dateFinishSpecified = true;
                    }

                    //var t = new tFactModification()
                    //{

                    //};

                    ///// Изменения факта назначения
                    //if ((int)infoAssigmentFact["typeWork"] == 1)
                    //{

                    //    assigmentFact.lastChanging = Convert.ToDateTime(infoAssigmentFact["lastChanging"]);
                    //    assigmentFact.previosID = infoAssigmentFact["baseID"].ToString();
                    //}

                    ///// Дата прекращение факта назначения
                    //if ((int)infoAssigmentFact["typeWork"] == 2)
                    //{
                    //    var termination = new FACT.tFactTermination()
                    //    {
                    //        uuid = infoAssigmentFact["ID"].ToString(),
                    //        assignmentFactUuid = infoAssigmentFact["baseID"].ToString(),
                    //        dateFinish = Convert.ToDateTime(infoAssigmentFact["dateFinish"].ToString()),
                    //        lastChanging = Convert.ToDateTime(infoAssigmentFact["lastChanging"])

                    //    };
                    //}

                    //Данные паспорта
                    var passportRFInfo = new PassportRFType()
                    {
                        Series = infoAssigmentFact["Series"].ToString(),
                        Number = infoAssigmentFact["Number"].ToString(),
                        IssueDate = Convert.ToDateTime(infoAssigmentFact["IssueDate"].ToString()),
                        Issuer = infoAssigmentFact["Issuer"].ToString(),
                    };

                    //Данные свидетельства о рождении
                    var passportSovietInfo = new SovietPassportType()
                    {
                        Series = infoAssigmentFact["Series"].ToString(),
                        Number = infoAssigmentFact["Number"].ToString(),
                        IssueDate = Convert.ToDateTime(infoAssigmentFact["IssueDate"].ToString()),
                        Issuer = infoAssigmentFact["Issuer"].ToString(),
                    };

                    //Тип паспорта
                    var docINFO = fACTHelper.GetPassportType(infoAssigmentFact);

                    //адрес
                    var addressInfo = new tAddress()
                    {
                        Region = infoAssigmentFact["Region"].ToString(),
                        City = infoAssigmentFact["City"].ToString(),
                        District = infoAssigmentFact["District"].ToString(),
                        OKSMCode = infoAssigmentFact["OKSMCode"].ToString(),
                        PostIndex = infoAssigmentFact["PostIndex"].ToString(),
                        Apartment = infoAssigmentFact["Apartment"].ToString(),
                        Street = infoAssigmentFact["Street"].ToString(),
                        House = infoAssigmentFact["House"].ToString(),
                        Housing = infoAssigmentFact["Housing"].ToString(),

                    };

                    var address = new tPersonInfoAddress()
                    {
                        PermanentResidence = addressInfo
                    };

                    //Группировака для вставки всех людей в блок данных reason_persons
                    var groupsReasonPersons = dt.AsEnumerable().GroupBy(r => new
                    {
                        ID = r.Field<int>("Person_id"),
                        IDCategory = r.Field<int>("id_familyUD")
                    })
                        .Select(grp => grp.First()).Where(x => (x.Field<int>("id_familyUD") == (int)infoAssigmentFact["id_familyUD"]));


                    var personInfoList = new List<tPersonInfo>();
                    foreach (var item in groupsReasonPersons)
                    {
                        if (groupsReasonPersons.Count() > 1)
                        {
                            personInfoList.Add(fACTHelper.GetPesonIfo(item, docINFO, address));
                        }
                    }

                    if (groupsReasonPersons.Count() > 1)
                    {
                        assigmentFact.reasonPersons = personInfoList.ToArray();
                    }

                    ///Вставка всех кто находится в семье. Они все получатели.

                    assigmentFact.mszReceiver = fACTHelper.GetPesonIfo(infoAssigmentFact, docINFO, address);

                    packageList.Add(assigmentFact);
                }

                var packageElements = new tPackageElements()
                {
                    fact = packageList.ToArray(),
                };

                var newPackage = new tPackage()
                {
                    packageId = packGuid.ToString(),
                    elements = packageElements,
                };

                var dataPackage = new data()
                {
                    package = newPackage,
                };

                return dataPackage;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw ex;
            }
        }
    }
}
