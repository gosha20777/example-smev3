﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SignatureHelper.SignatureCrypto;
using SignatureHelper.FolderFileOperation;
using SignatureHelper.DataBaseOperations;
using SignatureHelper.SaveFilesXML.PathNameFormat;

namespace EGISSO
{
    class CreatelocalMSZ
    {
        private string pathFolder;

        public CreatelocalMSZ(string pathFolder)
        {
            this.pathFolder = pathFolder;
        }

        public void GetLocalMSZ()
        {
            var dt = GetSQLDataMSZ();
            var fileNameXML = new FileNameXML();

            DataTable collapseTable = dt.Clone();

            //локальные меры
            var groupsLocalMsz = dt.AsEnumerable().GroupBy(r => new { ID = r.Field<Guid>("ID"), id_work = r.Field<int>("id_work") })
            .Select(grp => grp.First());

            //Категории локальных мер
            var groupsCategoriesMsz = dt.AsEnumerable().GroupBy(r => new { ID = r.Field<Guid>("ID"), IDCategory = r.Field<Guid>("IDCategory") })
            .Select(grp => grp.First());

            //Софинансирование локальных мер
            var groupsFoundingSourceMsz = dt.AsEnumerable().GroupBy(r => new
            { ID = r.Field<Guid>("ID"), id_work = r.Field<int>("id_work") })
            .Select(grp => grp.First());

            //Нормативно правовой акт 
            var groupsNPAMsz = dt.AsEnumerable().GroupBy(r => new
            { ID = r.Field<Guid>("ID"), NumberNPA = r.Field<string>("number"), DateNPA = r.Field<DateTime>("date") })
            .Select(grp => grp.First());


            int i = 0;
            
            ///Заполнение локальномеры в одном файле она может быть только одна
            foreach (var rowlocalMSZ in groupsLocalMsz)
            {

                fileNameXML.CodeOrganization = rowlocalMSZ.Field<string>("short_kod").ToString();
                fileNameXML.CodeFile = rowlocalMSZ.Field<string>("name_extension").ToString();

                i++;
                var localMSZ2 = GetlocalMSZ2(rowlocalMSZ);
                
                var classificationKMSZ = GetclassificationKMSZ(rowlocalMSZ);

                localMSZ2.classificationKMSZ = classificationKMSZ;

                //Заполнения списка категорий, в xml тип данных array[]

                var localCategoriesList = GetlocalCategoriesList(rowlocalMSZ, groupsCategoriesMsz);

                classificationKMSZ.localCategories = localCategoriesList.ToArray();

                ///Заполнение списка Софинансирование локальных мер в xml тип данных array[]
                ///
                var localfundingSourceList = GetlocalfundingSourceList(rowlocalMSZ, groupsFoundingSourceMsz);

                classificationKMSZ.cofinancing = localfundingSourceList.ToArray();

                ///Заполнение код страны локальных мер в xml тип данных array[]
                var codeOKTMO = new string[]
                {
                        rowlocalMSZ["codeOKTMO"].ToString()
                };

                localMSZ2.territories = codeOKTMO;
                if (rowlocalMSZ["estimation"] != null)
                {
                    localMSZ2.estimation = rowlocalMSZ["estimation"].ToString();
                }
               

                //Нормативно правовой акт локальных мер в xml тип данных array[]
                var localNPAList = GetlocalNPAList(rowlocalMSZ, groupsNPAMsz);

                localMSZ2.reasons = localNPAList.ToArray();

                localMSZ2.KBKCode = rowlocalMSZ["KBKCode"].ToString();


                //По идее можно формировать несколько лмсз но написано так что одна ЛМСЗ один файл. 
                //Из за этого в коллекции хранится одно значение
                //Пакет запроса
                var packageList = new List<LMSZ.tLocalMSZ2>();
                var newPackage = new LMSZ.tPackage()
                {
                    packageId = rowlocalMSZ["packageID"].ToString(),
                    elements = new LMSZ.tPackageElements()
                };             

                packageList.Add(localMSZ2);

                newPackage.elements.localMSZ = packageList.ToArray();

                if ((int)rowlocalMSZ["typeWork_LMSZ"] == 2)
                {
                    var invalidationList = new List<LMSZ.tLocalMSZInvalidation>();
                    var invalidationElements = new LMSZ.tLocalMSZInvalidation()
                    {
                        id = rowlocalMSZ["ID"].ToString(),
                        localMszId = rowlocalMSZ["baseID"].ToString()
                    };

                    newPackage.elements.invalidation = invalidationList.ToArray();
                }
                

               //Запрос с папкой и локальной мерой
               var request = new LMSZ.request()
                {
                    package = newPackage
                };

                //}

            
                var xmlOperation = new XMLOperation();
                var dataBaseOperation = new DataBaseOperation();
                
                xmlOperation.SaveXmlFile(request, xmlOperation.GetFormatName( $"{fileNameXML.FileName}",i),
                    PathSettings.PathToMailservlMSZ(), FolderNameEnum.Create);

                var xmlReport = xmlOperation.GetXMLProtocol<LMSZ.request>(request, request.package.packageId);

                dataBaseOperation.InsertToBase(xmlReport, request.package.packageId);
            }
            //MessageBox.Show($"XML файлы сформированы в количестве - {i} шт", "Формирования xml - ЛМСЗ", 
            //    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }



       private LMSZ.tLocalMSZ2 GetlocalMSZ2(DataRow rowlocalMSZ)
        {
            
            var localMSZ2 = new LMSZ.tLocalMSZ2()
            {
                ID = rowlocalMSZ["ID"].ToString(),
                code = rowlocalMSZ["code"].ToString(),
                title = rowlocalMSZ["title"].ToString(),
                dateEnact = Convert.ToDateTime(rowlocalMSZ["dateEnact"]),
                periodicityCode = rowlocalMSZ["periodicityCode"].ToString(),
                
                //,KBKCode = rowlocalMSZ["KBKCode"].ToString()
            };

            switch (rowlocalMSZ["typeWork_LMSZ"])
            {
                //Модификация ЛМСЗ
                case 1:
                    localMSZ2.lastChanging = Convert.ToDateTime(rowlocalMSZ["lastChanging"]);
                    localMSZ2.previousID = rowlocalMSZ["baseID"].ToString();
                    break;
                default:
                    break;
            }

            return localMSZ2;
        }

        private List<LMSZ.tLocalMSZNPA> GetlocalNPAList(DataRow rowlocalMSZ, IEnumerable<DataRow> groupsNPAMsz)
        {
            var localNPAList = new List<LMSZ.tLocalMSZNPA>();
            foreach (var localNPA in groupsNPAMsz)
            {
                if (localNPA["ID"].Equals(rowlocalMSZ["ID"]))
                {
                    var nPA = new LMSZ.tLocalMSZNPA()
                    {
                        number = localNPA["number"].ToString(),
                        date = Convert.ToDateTime(localNPA["date"]),
                        title = localNPA["titleNPA"].ToString(),
                        authority = localNPA["authority"].ToString(),
                        URL = localNPA["URL"].ToString(),
                    };
                    localNPAList.Add(nPA);
                }
            }
            return localNPAList;
        }

        private List<LMSZ.tLocalMSZClassificationKMSZFundingSource> GetlocalfundingSourceList(DataRow rowlocalMSZ, IEnumerable<DataRow> groupsFoundingSourceMsz)
        {
            var localfundingSourceList = new List<LMSZ.tLocalMSZClassificationKMSZFundingSource>();
            foreach (var localfoundingSource in groupsFoundingSourceMsz)
            {
                if (localfoundingSource["ID"].Equals(rowlocalMSZ["ID"]) && localfoundingSource["id_work"].Equals(rowlocalMSZ["id_work"]))
                {

                    var fundingSource = new LMSZ.tLocalMSZClassificationKMSZFundingSource()
                    {
                        codeFundingSource = localfoundingSource["codeFundingSource"].ToString(),
                        quota = Convert.ToDecimal(localfoundingSource["quota"])
                        
                    };
                    localfundingSourceList.Add(fundingSource);
                }
            }
            return localfundingSourceList;
        }

        private List<LMSZ.tLocalMSZClassificationKMSZLocalCategory> GetlocalCategoriesList(DataRow rowlocalMSZ, IEnumerable<DataRow> groupsCategoriesMsz)
        {
            var localCategoriesList = new List<LMSZ.tLocalMSZClassificationKMSZLocalCategory>();

            foreach (var rowslocalcategory in groupsCategoriesMsz)
            {
                if (rowslocalcategory["ID"].Equals(rowlocalMSZ["ID"]))
                {

                    var localCategory = new LMSZ.tLocalMSZClassificationKMSZLocalCategory()
                    {
                        ID = rowslocalcategory["IDCategory"].ToString(),
                        code = rowslocalcategory["code1"].ToString(),
                        title = rowslocalcategory["title1"].ToString(),
                        codeCategoryKMSZ = rowslocalcategory["codeCategoryKMSZ"].ToString()
                    };

                    localCategoriesList.Add(localCategory);

                }
            }
            return localCategoriesList;
        }

        private LMSZ.tLocalMSZClassificationKMSZ GetclassificationKMSZ(DataRow rowlocalMSZ)
        {
            var classificationKMSZ = new LMSZ.tLocalMSZClassificationKMSZ()
            {
                codePartKMSZ = rowlocalMSZ["codePartKMSZ"].ToString(),
                codeMSZ = rowlocalMSZ["codeMSZ"].ToString(),
                codeProvisionForm = rowlocalMSZ["codeProvisionForm"].ToString(),
                codeLevelNPA = rowlocalMSZ["codeLevelNPA"].ToString()
            };
            return classificationKMSZ;
        }

        public DataTable GetSQLDataMSZ()
        {    

            var dt = GetDataTable.GetQuery(GetDataTable.ConnectionString(), SqlQuery.QuerylocalMSZ);

            return dt;
        }

    }

}
