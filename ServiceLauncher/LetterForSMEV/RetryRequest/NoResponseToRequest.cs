﻿using Dapper;
using log4net;
using SignatureHelper.DataBaseOperations;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLauncher.LetterForSMEV.RetryRequest
{
    public class NoResponseToRequest
    {
        ILog _log;
        public NoResponseToRequest(ILog log)
        {
            _log = log;
        }

        /// <summary>
        /// Запросы которые отправлены в СМЭВ, поставлены в очередь, но нет от них ответа.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<dynamic> LostRequests()
        {
            try
            {
                string query =
                    @"
                    Select 
                            * 
                    from 
                        [dbo].[SMEV3LoggingRequest] slr
                    left join
                        [dbo].[SMEV3QueueRequests] sqr on slr.EnvelopeGuid = sqr.RequestGUID
                    where
                        ParseProtocolSuccess is null
                        ";

                using (var conn = new SqlConnection(GetDataTable.ConnectionString()))
                {
                    conn.Open();

                    return conn.Query(query);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }



    }
}
