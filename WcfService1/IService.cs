﻿using SMEV3Services.SMEV3;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Smev3Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    [ServiceKnownType(typeof(Response)),
        ServiceKnownType(typeof(MessageMetadata))]

    public interface IService
    {
        [OperationContract]
        string GetData(int value);

        /// <summary>
        /// FTP
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [OperationContract]
        CommonResponse RSID30271(ref CommonRequest req);

        // TODO: Add your service operations here
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "Smev3Services.ContractType".
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }

    public class CommonRequest
    {
        public CommonRequest() { }
        /// <summary>
        /// Конструктор для нового объекта с копированием служебных полей
        /// </summary>
        /// <param name="origin"></param>
        public CommonRequest(CommonRequest origin)
        {
            this.StateID = origin.StateID;            
        }

    
        private string _mailEgisso;
        [DataMember]
        public string MailEgisso
        {
            get { return _mailEgisso; }
            set { _mailEgisso = value; }
        }

        public string PathForSaveXML { get; set; }

        /// <summary>
        /// Идентификатор заявки (для асинхронных запросов)
        /// </summary>
        public string RequestID;

        /// <summary>
        /// Любая дополнительная информация вне установленных полей, 
        /// пока не используется, но все равно пусть будет.
        /// </summary>
        public string Aux;

        #region Служебные поля, не должны писаться в XML

        /// <summary>
        /// Успешность выполнения запроса: 
        /// 0 - не обработано, 1 - ошибка, 4 - повтор запроса
        /// </summary>
        public int StateID;
        /// <summary>
        /// Уникальный идентификатор, связанный с запросом 
        /// </summary>
        public string GUID;

        #endregion
    }
    public class CommonResponse
    {
        private object response;
        private DateTime dateRequest;
        private DateTime dateResponse;
        private string comment, stateRequest;

        private string dateList;
        private string tabNumber;
        private string listNumber;

        public CommonResponse() { }
        public CommonResponse(object response, DateTime dateRequest, DateTime dateResponse)
        {
            this.response = response;
            this.dateRequest = dateRequest;
            this.dateResponse = dateResponse;
        }

        /// <summary>
        /// Обработанный ответ на запрос
        /// </summary>
        public object Response
        {
            get
            {
                return response;
            }
            set
            {
                response = value;
            }
        }
        /// <summary>
        /// Дата и время отправки запроса
        /// </summary>
        public DateTime DateRequest
        {
            get
            {
                return dateRequest;
            }
            set
            {
                dateRequest = value;
            }
        }
        /// <summary>
        /// Дата и время ответа на запрос
        /// </summary>
        public DateTime DateResponse
        {
            get
            {
                return dateResponse;
            }
            set
            {
                dateResponse = value;
            }
        }

        /// <summary>
        /// Дата обращения из информационного листа
        /// </summary>
        public string DateList
        {
            get
            {
                return dateList;
            }
            set
            {
                dateList = value;
            }
        }
        /// <summary>
        /// Табельный номер инспектора, отправившего запрос
        /// </summary>
        public string TabNumber
        {
            get
            {
                return tabNumber;
            }
            set
            {
                tabNumber = value;
            }
        }
        /// <summary>
        /// Номер информационного листа
        /// </summary>
        public string ListNumber
        {
            get
            {
                return listNumber;
            }
            set
            {
                listNumber = value;
            }
        }

        /// <summary>
        /// Список номеров найденных запросов - дублей
        /// </summary>
        public List<int> DoubleRequests;

        /// <summary>
        /// Примечания к запросу/ответу
        /// </summary>
        public string Comment
        {
            get
            {
                return comment;
            }
            set
            {
                comment = value;
            }
        }

        /// <summary>
        /// Текущий статус заявки, к которой относится запрс
        /// </summary>
        public string StateRequest
        {
            get
            {
                return stateRequest;
            }
            set
            {
                stateRequest = value;
            }
        }
    }
    public class SMEVRequest
    {
        private string request = String.Empty;

        public SMEVRequest() { }

        /// <summary>
        /// Оригинальный текст SOAP запроса к СМЭВ
        /// </summary>
        public string Request
        {
            get
            {
                return request;
            }
            set
            {
                request = value;
            }
        }
    }

    public class Property
    {
        public Property() { }

        public string Name;
        public string Value;
    }

    public class Response
    {
        public Response() { }

        public object MethodResponse;

        public string Type;
        public List<Property> Properties = new List<Property>();
        public byte[] File;
        public string Error;

        public bool AddProperty(string propName, string propValue)
        {
            if (!String.IsNullOrEmpty(propName) && !String.IsNullOrEmpty(propValue) &&
                !String.IsNullOrEmpty(propValue.Trim(new char[] { '-', ' ' })))
            {
                Properties.Add(new Property()
                {
                    Name = propName,
                    Value = propValue
                });
                return true;
            }
            return false;
        }
    }
}
