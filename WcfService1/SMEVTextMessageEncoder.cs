﻿using log4net;
using SignatureHelper.ClassesFromXSD.Assigment_Fact;
using SignatureHelper.ClassesFromXSD.lonelyChildren.xsd;
using SignatureHelper.SignatureCrypto;
using Smev3Services;
using SMEV3Services.DataBase;
using System;
using System.IO;
using System.ServiceModel.Channels;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

/// <summary>
/// Кастомный кодировщик. Обертка над стандартным WCF кодировщиком.
/// 
/// </summary>
public class SMEVTextMessageEncoder : MessageEncoder
{
    private SMEVTextMessageEncoderFactory factory;
    private string contentType;
    private MessageEncoder innerEncoder;

    private string rsid;
    private CommonRequest cr;
    private SMEVRequest request;
    private int stackNum;
    private ILog _log;
    private MessageLogging _messageLogging;
    public SMEVTextMessageEncoder()
    {
        log4net.Config.XmlConfigurator.Configure();
        _log = LogManager.GetLogger(typeof(SMEVTextMessageEncoder));
        _messageLogging = new MessageLogging(_log);
    }

    public SMEVTextMessageEncoder(SMEVTextMessageEncoderFactory factory, string rsid, CommonRequest cr, SMEVRequest sr, int stackNum) : this()
    {
        this.factory = factory;
        this.innerEncoder = factory.InnerMessageFactory.Encoder;
        this.contentType = this.factory.MediaType;

        this.rsid = rsid;
        this.cr = cr;
        this.request = sr;
        this.stackNum = stackNum;

    }

    public override string ContentType
    {
        get
        {
            return this.contentType;
        }
    }

    public override string MediaType
    {
        get
        {
            return this.factory.MediaType;
        }
    }

    public override MessageVersion MessageVersion
    {
        get
        {
            return this.factory.MessageVersion;
        }
    }

    /// <summary/>
    /// <param name="buffer"/><param name="bufferManager"/><param name="contentType"/>
    /// <returns/>
    public override Message ReadMessage(ArraySegment<byte> buffer, BufferManager bufferManager, string contentType)
    {
        byte[] bytes1 = new byte[buffer.Count];
        Array.Copy((Array)buffer.Array, buffer.Offset, (Array)bytes1, 0, bytes1.Length);
        string response = System.Text.Encoding.UTF8.GetString(bytes1);

        XmlDocument xDoc = new XmlDocument();
        xDoc.LoadXml(response);
        try
        {
            //var dirName = $@"D:\temp\response_{DateTime.Now.ToString("HHmmssfff")}\";
            //Directory.CreateDirectory(dirName);
            //xDoc.Save(dirName + $@"{DateTime.Now.ToString("HHmmssfff")}.xml");
            var responseType = SMEVResponseType(xDoc);
            var responseSmev = XDocument.Parse(response).ToString();
            var xmlOperation = new XMLOperation();

            var mail = XDocument.Parse(cr.MailEgisso);          

            var mailInfo = new MailInfo(cr.MailEgisso);

            _messageLogging.SMEV3LoggingRequest(cr, request.Request, responseSmev, SMEVResponseType(xDoc), stackNum, mailInfo.PackageId, mailInfo.XDocMail);
            // Признак не логировать ошибки далее
            if (responseType == RespType.ERROR) cr.StateID = 1;
        }
        catch (Exception ex)
        {
            string Error = ex.Message;
            _log.Error(ex.GetBaseException().ToString());
            var responseSmev = XDocument.Parse(response).ToString();
            _messageLogging.SMEV3LoggingRequest(cr, request.Request, responseSmev, RespType.ERROR, stackNum, null, null);
        }

        string m = CorrectResponse(response);

        byte[] bytes2 = new UTF8Encoding().GetBytes(m);
        int length = bytes2.Length;
        int num1 = 0;
        int num2 = length + num1;
        byte[] array = bufferManager.TakeBuffer(num2);
        Array.Copy((Array)bytes2, 0, (Array)array, num1, num2);
        buffer = new ArraySegment<byte>(array, num1, num2);
        return this.innerEncoder.ReadMessage(buffer, bufferManager, contentType);
    }

    public class MailInfo
    {
        public XDocument XDocMail { get; set; }
        public string PackageId { get; set; }

        public MailInfo(string mail)
        {
            XDocMail = XDocument.Parse(mail);

            var xmlOperation = new XMLOperation();
            var mailInstance = xmlOperation.GetMailFromXML(mail);

            SetPackageId(mailInstance);
        }

        public void SetPackageId<T>(T mailInstance)
        {
            if (mailInstance is data)
            {
                var data = mailInstance as data;
                PackageId = data.package.packageId;
                return;
            }

            if (mailInstance is LonelyChildren.data)
            {
                var data = mailInstance as LonelyChildren.data;
                PackageId = data.package.packageId;
                return;
            }
        }              
    }

    /// <summary/>
    /// <param name="stream"/><param name="maxSizeOfHeaders"/><param name="contentType"/>
    /// <returns/>
    public override Message ReadMessage(Stream stream, int maxSizeOfHeaders, string contentType)
    {
        return this.innerEncoder.ReadMessage(stream, maxSizeOfHeaders, contentType);
    }

    /// <summary>
    /// Writes a message of less than a specified size to a byte array buffer at the specified offset
    /// 
    /// </summary>
    /// <param name="message"/><param name="maxMessageSize"/><param name="bufferManager"/><param name="messageOffset"/>
    /// <returns/>
    public override ArraySegment<byte> WriteMessage(Message message, int maxMessageSize, BufferManager bufferManager, int messageOffset)
    {
        byte[] array1 = this.innerEncoder.WriteMessage(message, maxMessageSize, bufferManager, messageOffset).Array;
        int length = array1.Length;
        string @string = Encoding.UTF8.GetString(array1);

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(@string);

        xmlDocument = CorrectRequest(xmlDocument);

        MemoryStream memoryStream = new MemoryStream();
        XmlWriter w = XmlWriter.Create((Stream)memoryStream);
        xmlDocument.Save(w);

        request.Request = xmlDocument.OuterXml;

        //Directory.CreateDirectory(dirName);

        //try { File.WriteAllText(dirName + $@"{DateTime.Now.ToString("HHmmssfff")}.xml", request.Request); }
        //catch { }

        w.Flush();
        byte[] buffer = memoryStream.GetBuffer();
        int num = (int)memoryStream.Position;
        memoryStream.Close();
        int bufferSize = num + messageOffset;
        byte[] array2 = bufferManager.TakeBuffer(bufferSize);
        Array.Copy((Array)buffer, 0, (Array)array2, messageOffset, num);
        return new ArraySegment<byte>(array2, messageOffset, num);
    }
    /// <summary>
    /// Writes a message to a specified stream
    /// 
    /// </summary>
    /// <param name="message"/><param name="stream"/>
    public override void WriteMessage(Message message, Stream stream)
    {
        this.innerEncoder.WriteMessage(message, stream);
    }

    private string CorrectResponse(string origin)
    {
        string result;
        switch (rsid)
        {
            default:
                result = origin;
                break;
        }
        return result;
    }
    private XmlDocument CorrectRequest(XmlDocument origin)
    {
        XmlDocument result;
        switch (rsid)
        {
            //case "20139":
            //    result = new XmlDocument();
            //    result.LoadXml(origin.OuterXml.Replace(sendActorAttributeName, receiveActorAttributeName));
            //    break;
            //case "20155":
            //    result = new XmlDocument();
            //    result.LoadXml(origin.OuterXml.Replace(sendActorAttributeName, receiveActorAttributeName));
            //    break;
            //case "20156":
            //    result = new XmlDocument();
            //    result.LoadXml(origin.OuterXml.Replace(sendActorAttributeName, receiveActorAttributeName));
            //    break;
            //case "20166":
            //    result = new XmlDocument();
            //    result.LoadXml(origin.OuterXml.Replace(" s:mustUnderstand=\"1\"", String.Empty));
            //    break;
            //case "20169":
            //    result = new XmlDocument();
            //    result.LoadXml(origin.OuterXml.Replace(sendActorAttributeName, receiveActorAttributeName));
            //    break;
            //case "10117":
            //case "10127":
            //case "20183":
            //case "20188":
            //    result = new XmlDocument();
            //    result.LoadXml(origin.OuterXml.Replace(" s:mustUnderstand=\"1\"", String.Empty));
            //    break;
            //case "20215":
            //    result = new XmlDocument();
            //    result.LoadXml(origin.OuterXml.Replace(sendActorAttributeName, receiveActorAttributeName));
            //    break;
            default:
                result = origin;
                break;
        }
        return result;
    }
    private RespType SMEVResponseType(XmlDocument xDoc)
    {
        RespType result = RespType.DATA;

        if (xDoc.GetElementsByTagName("Fault", "http://schemas.xmlsoap.org/soap/envelope/").Count > 0)
        {
            //if (rsid == "10011" && //sub 586
            //    xDoc.InnerXml.Contains("The content of element 'ответ' is not complete. One of '{выплаты}' is expected"))
            //    return RespType.DATA;
            //if (rsid == "10018" && xDoc.InnerXml.Contains("КодОбр=\"53\""))
            //    return RespType.DATA;
            //if (rsid == "10018" && xDoc.InnerXml.Contains("КодОбр=\"99\""))
            //    return RespType.OTHER;
            return RespType.ERROR;
        }

        return result;
    }
}