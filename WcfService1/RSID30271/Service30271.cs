﻿using CryptoPro.Sharpei;
using log4net;
using SignatureHelper.ClassesFromXSD.Assigment_Fact;
using SignatureHelper.FolderFileOperation;
using SignatureHelper.SignatureCrypto;
using Smev3Services;
using SMEV3Services.DataBase;
using SMEV3Services.SMEV3;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using System.Xml;

namespace SMEV3Services.RSID30271
{
    public class Service30271
    {
        private readonly ILog _log;
        private MessageLogging _messageLogging;
        public Service30271(ILog log)
        {
            _log = log;
            _messageLogging = new MessageLogging(_log);
        }

        private SMEV3.Smev3ProdPortTypeClient InitializationClient(CommonRequest req, SMEVRequest originalRequestMessage)
        {
            try
            {
                #region Client Settings

                var client = new SMEV3.Smev3ProdPortTypeClient();
                //client.Open();
                CustomBinding binding = new CustomBinding(client.Endpoint.Binding);
                binding.SendTimeout = new TimeSpan(0, 2, 0);
               
                SMEVMessageEncodingBindingElement ebe = new SMEVMessageEncodingBindingElement("30271", req, originalRequestMessage, 13);
                binding.Elements.Remove<MtomMessageEncodingBindingElement>();
                binding.Elements.Insert(0, ebe);

                client.Endpoint.Binding = binding;

                #endregion
                return client;
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }

        }

        private SenderProvidedRequestData RequestGeneration(string messageID, XmlDocument ftpXDoc, string hashFile, byte[] sigPKCS7)
        {
            try
            {
                SMEV3Services.SMEV3.SenderProvidedRequestData requestData = new SMEV3Services.SMEV3.SenderProvidedRequestData();
                requestData.Id = "SIGNED_BY_CALLER";
                requestData.MessageID = messageID;

                requestData.MessagePrimaryContent = ftpXDoc.DocumentElement;

                requestData.RefAttachmentHeaderList = new RefAttachmentHeaderList();

                requestData.RefAttachmentHeaderList.RefAttachmentHeader = new RefAttachmentHeaderType[]
                {
                    new SMEV3Services.SMEV3.RefAttachmentHeaderType()
                    {
                        uuid = requestData.MessageID,
                        Hash =  hashFile ,
                        MimeType = "application/xml",
                        SignaturePKCS7 = sigPKCS7
                    }
                };

                return requestData;
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        private void UploadFileToFtp(string messageID, string filePath)
        {
            try
            {
                FtpWebRequest ftpreq = (FtpWebRequest)WebRequest.Create(@"ftp://ftp.smev.vpn/" + messageID);
                ftpreq.Credentials = new NetworkCredential("anonymous", "anonymous@smev.vpn");
                ftpreq.Method = WebRequestMethods.Ftp.MakeDirectory;
                ftpreq.GetResponse();

                ftpreq = (FtpWebRequest)WebRequest.Create(@"ftp://ftp.smev.vpn/" + messageID + $@"/{messageID}.xml");
                ftpreq.Credentials = new NetworkCredential("anonymous", "anonymous@smev.vpn");
                ftpreq.Method = WebRequestMethods.Ftp.UploadFile;

                var fileContent = GetByteFile($@"{filePath}{messageID}\{messageID}.xml");
                ftpreq.ContentLength = fileContent.LongLength;

                using (Stream requestStream = ftpreq.GetRequestStream())
                {
                    requestStream.Write(fileContent, 0, fileContent.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }

                ftpreq.GetResponse();
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
                //if (ex.InnerException != null) Error += " " + ex.InnerException.Message;

                //File.WriteAllText($@"D:\temp\err_" + messageID + "_" + DateTime.Now.ToString("HHmmssfff") + ".log", Error);

                //if (req.StateID != 1)
                //{
                //    Connections.CheckAndLog(req, sr.Request, Error, true);
                //    req.StateID = 1;
                //}

                //result.Error = Error;
            }
        }

        //Отправка в СМЭВ3 сообщения с письмом и сформированным конвертом о том что есть файл в ФТП и передачи нашего запроса в очередь
        private CommonRequest SendRequestSMEV3(Smev3ProdPortTypeClient client, SenderProvidedRequestData requestData,
             XmlElement callerSignature, CommonRequest request, ref Smev3Services.Response responseResult)
        {
            try
            {
                XmlElement smevSignature;
                SMEV3Services.SMEV3.MessageMetadata mm = client.SendRequest(requestData, null, callerSignature, out smevSignature);

                responseResult.MethodResponse = mm;

                if (mm.Status == SMEV3Services.SMEV3.InteractionStatusType.requestIsQueued)
                {
                    request.RequestID = requestData.MessageID;
                    request.StateID = 0;
                    //MessageBox.Show($@"Отправлено сообщение с GUID - {request.RequestID}");
                    //Connections.LogAsync(request);
                }

                return request;

            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        public void SaveXmlToFolder(string fullPath, string stringContent)
        {
            Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
            File.WriteAllText(fullPath, stringContent);
        }

        public CommonResponse RunService30271(CommonRequest req)
        {
            var originalSmevRequest = new SMEVRequest();
            var client = InitializationClient(req, originalSmevRequest);
            var messageID = GuidGenerator.GenerateTimeBasedGuid().ToString();
            //var requestData = RequestGeneration(messageID);
            #region Data Settings

            var opretationXml = new XMLOperation();
            var serrialNumber = opretationXml.clientCertificate(PathSettings.SerrialNumber());         

            var createDirectory = Task.Factory.StartNew(() => Directory.CreateDirectory($@"{req.PathForSaveXML}{messageID}"));
            createDirectory.Wait();

            var xmlConvert = new XMLOperation();

            XmlDocument reqData = xmlConvert.SerializeXMLToDocument(typeof(FTP.request), new FTP.request() { attachment = true }, null);
            SaveXmlToFolder($@"{req.PathForSaveXML}{messageID}\{messageID}.xml", req.MailEgisso);

            //Формирование конверта с письмом в СМЭВ 3
            var requestData = RequestGeneration(messageID, reqData, GetHashFileBase64String($@"{req.PathForSaveXML}{messageID}\{messageID}.xml"),
                GetSignedSmc(GetByteFile($@"{req.PathForSaveXML}{messageID}\{messageID}.xml"), serrialNumber));

            var xd = xmlConvert.SerializeXMLToDocument(typeof(SMEV3Services.SMEV3.SenderProvidedRequestData), requestData, null);
            ///Подписывание сообщения перед отправкой
            XmlElement callerSignature = opretationXml.SignedData(xd, requestData.Id, serrialNumber.SerialNumber);
            xd.Save($@"{req.PathForSaveXML}SenderProvidedRequestData.xml");
            #endregion

            Smev3Services.Response responseResult = new Smev3Services.Response();

            try
            {
                //Выкладывание на фтп файла перед отправкой сообщения в СМЭВ 3
                UploadFileToFtp(messageID, req.PathForSaveXML);

                SendRequestSMEV3(client, requestData, callerSignature, req, ref responseResult);

                _messageLogging.SaveLogRequest(req);
            }

            catch (Exception ex)
            {
                string errorMessage = ex.GetBaseException().ToString();
                _log.Error(errorMessage);

                if (req.StateID != 1)
                {
                    _messageLogging.LoggingSMEV3Request(req, originalSmevRequest.Request, errorMessage, true);
                    req.StateID = 1;
                }

                responseResult.Error = errorMessage;
            }

            return new CommonResponse(responseResult, DateTime.Now, DateTime.Now);
        }

        private string GetSoapRequest()
        {
            try
            {
                OperationContext context = OperationContext.Current;

                if (context != null && context.RequestContext != null)
                {
                    System.ServiceModel.Channels.Message msg = context.RequestContext.RequestMessage;

                    return msg.ToString();
                }
                return "";
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        private byte[] GetSignedSmc(byte[] fileContent, X509Certificate2 serrialNumber)
        {
            try
            {
                ContentInfo cInfo = new ContentInfo(fileContent);
                SignedCms sCms = new SignedCms(cInfo, true);
                CmsSigner cmsSigner = new CmsSigner(serrialNumber);
                sCms.ComputeSignature(cmsSigner, false);
                return sCms.Encode();
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        private string GetHashFileBase64String(string filePath)
        {
            try
            {
                byte[] fileContent = File.ReadAllBytes(filePath);
                byte[] hash = Gost3411_2012_256.Create().ComputeHash(fileContent);
                return Convert.ToBase64String(hash);
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        private byte[] GetByteFile(string FilePath)
        {
            try
            {
                return File.ReadAllBytes(FilePath);
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }
    }
}