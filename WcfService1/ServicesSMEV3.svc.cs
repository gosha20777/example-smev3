﻿using CryptoPro.Sharpei;
using SMEV3Services;
using System;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Xml;
using SMEV3Services.SMEV3;
using System.Windows.Forms;
using System.Security.Cryptography.Pkcs;
using SignatureHelper.SignatureCrypto;
using SignatureHelper.FolderFileOperation;
using SMEV3Services.RSID30271;
using log4net;
using Autofac;

namespace Smev3Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class ServicesSMEV3 : IService
    {
        private readonly ILog _log;
        private Service30271 _service302071;
        public ServicesSMEV3(ILog log, Service30271 service302071)
        {
            _log = log;
            _service302071 = service302071;
        }
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }

            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        //Ftp-хранилище
        #region RSID30271

        public CommonResponse RSID30271(ref CommonRequest req)
        {
            try
            {
                _log.Info("Service - RSID302071 Run");

                return _service302071.RunService30271(req);
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }
    }
    #endregion
}
